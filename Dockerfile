FROM node:6-alpine

RUN mkdir /opt/build-moinitor

COPY . /opt/build-moinitor/

RUN ls -laR /opt/build-moinitor/

WORKDIR /opt/build-moinitor

RUN npm install && \
    npm run build

ENTRYPOINT ["npm", "run", "start"]
