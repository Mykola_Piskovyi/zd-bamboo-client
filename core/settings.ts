const sec = (num: number = 1) => (num * 1000);
const minutes = (num: number = 1) => (num * sec(60));

const APP_SETTINGS = {

	APP_NAME: 'build-notifier',

	BAMBOO: {
		HOST_PORT: 'http://bamboo.zoomdata.com:8085',
		HOST: 'https://bamboo.zoomdata.com',
		COOKIE_NAME: 'BAMBOO-CLIENT_',
		CATCH_PLANS: ['regression', 'upgrade'],
		SKIP_PLANS: ['daily']
	},

	LINKS: {
		BAMBOO_BUILD_BROWSE: 'https://bamboo.zoomdata.com/browse',
		JIRA_TICKET_BROWSE: 'https://zoomdata.atlassian.net/browse',
		BITBUCKET_BRANCH_BROWSE: 'https://bitbucket.org/zoomdata/zoomdata/branch'
	},

	BUILDS: {
		REFRESH_TIME_PERIOD: minutes(1),
		REFRESH_STATUS_AFTER_RUN_BUILD: sec(3),
		NOTIFICATION_PERIOD: sec(5),
		CHECK_BUILDS_STATUSES: minutes(4)
	}

};
export = APP_SETTINGS;
