# Zoomdata Bamboo Builds Monitor

## Requirements
- node > v6.9
- npm

## Install
```sh
$ git clone [git-repo-url]
$ cd zd-bamboo-client
$ npm install
$ npm run build
$ npm run start
```
This will set up a server at 1234 port (default, or use commant `"npm run start -- --port=4000"` for run on port 4000)

## Docker
Use
```sh
git clone [git-repo-url]
cd zd-bamboo-client
docker build -t zd-bamboo-client:latest .
```
to prepare corresponding Docker image

