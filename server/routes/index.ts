'use strict';
import * as path from 'path';
import {Express, Router} from 'express';

import * as utils from '../utils';

import applyBambooRoutes from './Bamboo';

const routes = Router();

routes.use(utils.router.allowCrossDomain);

routes.get('/', (req, resp, next) => onLoadIndex(resp, next));
routes.get('/test', (req, resp, next) => resp.send('Test page.'));

routes.use('/bamboo', applyBambooRoutes);

export default routes;

function onLoadIndex(resp, next) {
	const filePath = path.resolve(__dirname, '../index.html');
	resp.sendFile(filePath);
};
