'use strict';
import {Router, Request} from 'express';
import {assign} from 'lodash';
import * as fetch from 'node-fetch';
import * as qs from 'querystring';
import {auth, logger} from '../../utils';

import {
	onGetBamboo, onAuthBamboo,
	getAllPlans, runBuildPlan, getBranchPlan, 
	getLatestPlanStatus, getLatestPlanNumInfo, getLatestResults,
	createBranchPlan, 
	getQuickDataSearch,
	getProjectSearch,
	getBranchesSearch,
	getPlanSearch,
	getVcsBranches,
	removeQueueBuild
} from './bambooHandlers';

const routes = Router();

// README: All details for REST equal docs according to Bamboo REST
// https://developer.atlassian.com/bamboodev/rest-apis/bamboo-rest-resources

routes.get('/',  (req, resp, next) => onGetBamboo(req, resp));
routes.post('/', (req, res) => (onAuthBamboo(req, res)));

routes.get('/plan',  (req, resp, next) => getAllPlans(req, resp));
routes.post('/plan/:planKey', (req, resp) => runBuildPlan(req, resp));

routes.get('/plan/:projectKey/branch/:shortName', (req, resp) => (getBranchPlan(req, resp)));
routes.put('/plan/:projectKey/branch/:shortName', (req, resp) => (createBranchPlan(req, resp)));

routes.get('/plan/:projectKey/vcsBranches', (req, resp) => (getVcsBranches(req, resp)))

routes.get('/result/:planKey', (req, res) => getLatestResults(req, res));
routes.get('/result/:planKey/:planNum', (req, res) => getLatestPlanNumInfo(req, res));
routes.get('/result/status/:planKey', (req, res) => getLatestPlanStatus(req, res));

routes.get('/quicksearch', (req, res) => getQuickDataSearch(req, res));
routes.get('/search/projects', (req, res) => getProjectSearch(req, res));
routes.get('/search/branches', (req, res) => getBranchesSearch(req, res));
routes.get('/search/plan', (req, res) => getPlanSearch(req, res));

routes.delete('/queue/:planKeyBuildNumber', (req, res) => (removeQueueBuild(req, res)));

export default routes;