'use strict';

import {assign, isEmpty, includes, isError, attempt} from 'lodash';
import {Router, Request} from 'express';
import * as fetch from 'node-fetch';
import * as qs from 'querystring';

import * as APP_SETTINGS from '../../../core/settings';
import {auth, logger, string} from '../../utils';

const log = logger(__filename);
const queryParams = (params: Object) => (isEmpty(params) ? '' : ('?' + qs.stringify(params)));

const CRED = {
	HOST_PORT: APP_SETTINGS.BAMBOO.HOST_PORT,
	COOKIE_NAME: APP_SETTINGS.BAMBOO.COOKIE_NAME
};

const HEADERS = {
	ACCEPT: {'Accept': 'application/json'},
	CONTENT: {'Content-Type': 'application/json'},
	AUTH_USER: (login, pass) => ({'Authorization': auth.authenticateUser(login, pass)}),
	COOKIE: (req) => {
		return req.cookies ? {
				'Cookie': bambooCookie.getFromApp(req.cookies),
				'X-Atlassian-Token': 'no-check'
			} : {};
	}
};

const bambooCookie = {
	/**
	 * cookie "JSESSIONID=2A9F39C316D1B893EB9CC92A421C89C7"
	 * return ["BAMBOO-CLIENT_JSESSIONID", "2A9F39C316D1B893EB9CC92A421C89C7"]
	 */
	getToApp: (cookie) => {
		const cookies = cookie ? cookie.split('; ') : null;
		const setCookie = cookies ? cookies[0].split('=') : null;
		return setCookie ? [[CRED.COOKIE_NAME, setCookie[0]].join(''), setCookie[1]] : [];
	},
	/**
	 * cookie {"BAMBOO-CLIENT_JSESSIONID": "2A9F39C316D1B893EB9CC92A421C89C7"}
	 * return "JSESSIONID=2A9F39C316D1B893EB9CC92A421C89C7"
	 */
	getFromApp: (bambCookie: Object) => {
		const bambooName = Object.keys(bambCookie).filter(key => (key.match(CRED.COOKIE_NAME)))[0];
		const exist = includes(Object.keys(bambCookie), bambooName);
		return exist ? [bambooName.split(CRED.COOKIE_NAME)[1], bambCookie[bambooName]].join('=') : '';
	}
};

const buildUrl = (uri: string) => (CRED.HOST_PORT + '/rest/api/latest' + uri);
const headers = (headers = {}) => (assign({}, HEADERS.ACCEPT, HEADERS.CONTENT, headers));

const proxyResponse = (originRes, respData) => {
	if (respData.ok) {
		return respData.text().then(text => {
			const setCookie = bambooCookie.getToApp(respData.headers.get('set-cookie'));
			if (!isEmpty(setCookie)) {
				originRes.cookie(...setCookie);
			}
			const result = isError(attempt(() => JSON.parse(text))) ? 
				text : 
				JSON.parse(text);
			originRes.status(respData.status).send(result);
		});
	} else {
		return respData.text().then(text => {
			const result = isError(attempt(() => JSON.parse(text))) ? 
				[`${respData.status}: ${respData.statusText}`, string.escapeHtmlInText(text)].join('\n') :
				JSON.parse(text);
			originRes.status(respData.status).send(result);
		});
	}
};

export {
	onGetBamboo,
	onAuthBamboo,
	getAllPlans,
	runBuildPlan,		// POST
	getBranchPlan,
	getLatestResults,
	getLatestPlanNumInfo,
	getLatestPlanStatus,
	getVcsBranches,
	createBranchPlan,	// PUT
	getQuickDataSearch,
	getProjectSearch,
	getBranchesSearch,
	getPlanSearch,
	removeQueueBuild
}

function onGetBamboo(req: Request, res) {
	const ip = (req.headers['x-forwarded-for'] || '').split(',')[0] || req.connection.remoteAddress;
	const info = ip.replace(/^.*:/, '') + ' => ' + new Date().toString()
	
	log('GET: ' + info);
	const cookies = bambooCookie.getFromApp(req.cookies);

	fetch(
		buildUrl('/plan' + '?os_authType=basic'), {
			method: 'GET',
			headers: headers(HEADERS.COOKIE(req)),
			credentials: 'include'
		})
		.then((respData) => {
			return proxyResponse(res, respData);
		})
		.catch((error) => (onError(res, error)));
};

/**
 * 
 * @param req 
 * req.body.username: string <btoa>
 * req.body.password: string <btoa>
 * @param res 
 */

function onAuthBamboo(req, res) {
	const uri = buildUrl('/?os_authType=basic');
	const username = auth.atob(req.body.username);
	const password = auth.atob(req.body.password);
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers(HEADERS.AUTH_USER(username, password)),
		mode: 'cors'
	};
	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function getAllPlans(req, res) {
	const uri = buildUrl('/plan' + `${queryParams(req.query)}`);
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers()
	};

	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function removeQueueBuild(req, res) {
	log('DELETE ' + req.params.planKeyBuildNumber);

	const {planKeyBuildNumber} = req.params;
	const deleteOpts: RequestInit = {
		method: 'DELETE',
		headers: headers(HEADERS.COOKIE(req)),
		mode: 'cors',
		credentials: 'include'
	};

	const uri = buildUrl(`/queue/${planKeyBuildNumber}`);
	fetch(uri, deleteOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function runBuildPlan(req, res) {
	log('POST ' + req.params.planKey);

	const {planKey} = req.params;
	const postOpts: RequestInit = {
		method: 'POST',
		headers: headers(HEADERS.COOKIE(req)),
		mode: 'cors',
		credentials: 'include'
	};

	const uri = buildUrl(`/queue/${planKey}`);
	fetch(uri, postOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function getLatestResults(req, res) {
	const {planKey} = req.params;
	const uri = buildUrl(`/result/${planKey}${queryParams(req.query)}`);
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers(),
		credentials: 'include'
	};

	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function getLatestPlanNumInfo(req, res) {
	const {planKey, planNum} = req.params;
	const uri = buildUrl(`/result/${planKey}/${planNum}${queryParams(req.query)}`);
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers()
	};

	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function getLatestPlanStatus(req, res) {
	const {planKey} = req.params;
	const uri = buildUrl(`/result/status/${planKey}` + `${queryParams(req.query)}`);
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers(),
		credentials: 'include'
	};

	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function getBranchPlan(req, res) {
	const {projectKey, shortName} = req.params;
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers(),
		credentials: 'include'
	};

	const uri = buildUrl(`/plan/${projectKey}/branch/${shortName}` + `${queryParams(req.query)}`);
	
	log('GET', uri);
	
	// 200 - The newly created branch.
	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function createBranchPlan(req, res) {
	const {projectKey, shortName} = req.params;
	const vcsBranch = req.query;
	const putOpts: RequestInit = {
		method: 'PUT',
		headers: headers(HEADERS.COOKIE(req)),
		mode: 'cors',
		credentials: 'include'
	};

	const uri = buildUrl(`/plan/${projectKey}/branch/${shortName}` + `${queryParams(req.query)}`);
	log('PUT', uri);
	
	// 200 - The newly created branch.
	fetch(uri, putOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function getVcsBranches(req, res) {
	const {projectKey} = req.params;
	const uri = buildUrl(`/plan/${projectKey}/vcsBranches` + `${queryParams(req.query)}`);
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers(),
		credentials: 'include'
	};

	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function getQuickDataSearch(req, res) {
	const uri = buildUrl(`/quicksearch${queryParams(req.query)}}`);
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers(),
		credentials: 'include'
	};

	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function getProjectSearch(req, res) {
	const uri = buildUrl(`/search/projects${queryParams(req.query)}}`);
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers(HEADERS.COOKIE(req)),
		credentials: 'include'
	};

	log('GET/search/projects', queryParams(req.query));

	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function getBranchesSearch(req, res) {
	const uri = buildUrl(`/search/branches${queryParams(req.query)}}`);
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers(HEADERS.COOKIE(req)),
		credentials: 'include'
	};

	log('GET/search/branches', queryParams(req.query));

	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function getPlanSearch(req, res) {
	const uri = buildUrl(`/search/plans${queryParams(req.query)}}`);
	const getOpts: RequestInit = {
		method: 'GET',
		headers: headers(HEADERS.COOKIE(req)),
		credentials: 'include'
	};

	log('GET/search/plans', queryParams(req.query));

	fetch(uri, getOpts)
		.then((respData) => (proxyResponse(res, respData)))
		.catch((error) => (onError(res, error)));
}

function onError(resp, error) {
	log('ERROR', JSON.stringify(error));
	resp.sendStatus(400, error);
}