import {Response, Request} from 'express';
import {partial} from 'lodash';

import string from './string';

const IS_PROD = process.env.NODE_ENV === 'production';

const router = {
	allowCrossDomain
};

const auth = {
	btoa: btoa,
	atob: atob,
	authenticateUser: (user, password) => {
		return 'Basic ' + btoa(user + ":" + password);
	}
}

const isProd = () => (IS_PROD);

const log = (...args) => (IS_PROD ? null :console.log(...args));
const logger = (filename) => {
	const Reset = '\x1b[0m';
	const FgGreen = '\x1b[32m';
	const FgRed = '\x1b[31m';
	return (...args) => log(FgGreen, filename, Reset, ...args);
};

export {
	log,
	logger,
	isProd,
	router,
	auth,
	string
}

function allowCrossDomain (req: Request, res: Response, next) {
  	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	if ('OPTIONS' === req.method) {
		res.send(200);
	} else {
		next();
	}
};

function btoa(string) {
	return new Buffer(string).toString('base64');
}

function atob(b64Encoded) {
	return new Buffer(b64Encoded, 'base64').toString();
}