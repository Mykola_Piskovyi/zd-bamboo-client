const string = {
	escapeHtmlInText: escapeHtmlInText
};

export default string;

function escapeHtmlInText(html) {
    //-- remove BR tags and replace them with line break
	let returnText = html
		.replace(/<br>/gi, "\n")
		.replace(/<br\s\/>/gi, "\n")
		.replace(/<br\/>/gi, "\n");

    //-- remove P and A tags but preserve what's inside of them
	returnText = returnText
		.replace(/<p.*>/gi, "\n")
		.replace(/<a.*href="(.*?)".*>(.*?)<\/a>/gi, " $2 ($1)");

    //-- remove all inside SCRIPT and STYLE tags
	returnText = returnText
		.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "")
		.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
    //-- remove all else
	returnText = returnText
		.replace(/<(?:.|\s)*?>/g, "");

    //-- get rid of more than 2 multiple line breaks:
	returnText = returnText
		.replace(/(?:(?:\r\n|\r|\n)\s*){2,}/gim, "\n\n");

    //-- get rid of more than 2 spaces:
	returnText = returnText
		.replace(/ +(?= )/g,'');

    //-- get rid of html-encoded characters:
	returnText = returnText
		.replace(/&nbsp;/gi," ")
		.replace(/&amp;/gi,"&")
		.replace(/&quot;/gi,'"')
		.replace(/&lt;/gi,'<')
		.replace(/&gt;/gi,'>');
	
	return returnText;
}