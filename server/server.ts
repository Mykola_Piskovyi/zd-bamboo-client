import * as express from 'express';
import * as path from 'path';
import {assign} from 'lodash';
import * as fetch from 'node-fetch';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as yargs from 'yargs';

import routes from './routes';

const flags = yargs.argv;
const PORT = flags.port || 1234;

const app = express();

app.use(express.static('dist'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use(routes);

app.listen(PORT, () => console.log(`listening on port ${PORT}`));