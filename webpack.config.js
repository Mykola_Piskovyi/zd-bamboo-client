var path = require('path');
var webpack = require('webpack');
var nodeExternals = require('webpack-node-externals');
var Clean = require('clean-webpack-plugin');
var Copy = require('copy-webpack-plugin');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var TARGET_POINT = process.env.TARGET_POINT;
var NODE_MODE = process.env.NODE_ENV || 'development'; 
var IS_PROD = NODE_MODE === 'production';

var INPUT_CLIENT = path.resolve(__dirname, './src/index.ts');
var INPUT_SERVER = path.resolve(__dirname, './server/server.ts');
var OUT_PATH = 'dist';

var clientPolyfill = ['whatwg-fetch'];

var rules = [
	{
		test: /\.tsx?$/,
		exclude: [/node_modules/],
		loader: 'ts-loader'
	}
];

var resolve = {
	extensions: ['.tsx', '.ts', '.js']
};

var plugins = [
	new webpack.DefinePlugin({
        'process.env': {'NODE_ENV': JSON.stringify(NODE_MODE)}
    })
];

console.log('process.env.NODE_ENV: ' + NODE_MODE);

// ----- CONFIGS

var clientConfig = {
	target: 'web',
	entry: [...clientPolyfill, INPUT_CLIENT],
	output: {
    	path: path.resolve(OUT_PATH),
		filename: 'app.bundle.js'
	},
	devtool: IS_PROD ? 'nosources-source-map' : 'eval-source-map',
	module: {
		rules: rules.concat([{
			test: /\.scss$/,
			exclude: /node_modules/,
			loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader!sass-loader'})
		}])
	},
	resolve: resolve,
	plugins: plugins
		.concat([
			new Clean([OUT_PATH]),
			new Copy([
				{ from: './index.html' },
				{ from: './favicon.ico' },
				{ from: './node_modules/semantic-ui-css/semantic.css' },
				{ from: './node_modules/semantic-ui-css/themes', to: 'themes' }
			]),
			new ExtractTextPlugin({
				filename: 'main.css',
				allChunks: true
			})
		])
};

var serverConfig = {
	target: 'node',
	entry: INPUT_SERVER,
	externals: [nodeExternals()],
	context: __dirname,
	node: {
		__filename: true,
		__dirname: true
	},
	output: {
    	path: path.resolve(__dirname, OUT_PATH),
		filename: 'server.bundle.js',
		libraryTarget: 'commonjs'
	},
	devtool: 'eval-source-map',
	module: {rules: rules},
	resolve: resolve,
	plugins: plugins
};

var exports =  [clientConfig, serverConfig];

if (TARGET_POINT === 'SERVER') {
	exports = [serverConfig];
}
if (TARGET_POINT === 'CLIENT') {
	exports = [clientConfig];
}

module.exports = exports;