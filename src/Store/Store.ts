import {Store, createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reducers, {ApplicationState} from '../Reducers/index';

const middlewares = [thunk];
export const store = createStore(
	reducers, applyMiddleware(...middlewares)
) as Store<ApplicationState>;