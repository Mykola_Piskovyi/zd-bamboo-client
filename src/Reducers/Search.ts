import {assign, without, pick} from 'lodash';
import LocalStorage from '../Service/LocalStorage';

import {APP_ACTIONS} from '../Actions/App.actions';
import {BUILD_ACTIONS, BuildBranch} from '../Actions/Builds.actions';
import {CHAIN_BUILDS_ACTIONS} from '../Actions/ChainBuilds.actions';

type SearchState = {
    branch: Array<BuildBranch>;
    plan: Array<any>;
}
const initialState = {
    branch: [],
    plan: []
};

type Action = {
    type: string;
    data: BuildBranch;
};

function Search(state = initialState, action: Action) {
    switch (action.type) {
        case BUILD_ACTIONS.ADD_ITEM:
            const branch = state.branch.filter(item => (item.key !== action.data.key));
            return {...state, branch: branch};
        case BUILD_ACTIONS.SEARCH_RESPONSE:
            return {...state, branch: action.data};
        case BUILD_ACTIONS.SEARCH_REMOVE:
            return {...state, branch: []};

        case CHAIN_BUILDS_ACTIONS.ADD_ITEM:
            const plan = state.plan.filter(item => (item.key !== action.data.key));
            return {...state, plan: plan};
        case CHAIN_BUILDS_ACTIONS.SEARCH_RESPONSE:
            return {...state, plan: action.data};
        case CHAIN_BUILDS_ACTIONS.SEARCH_REMOVE:
        case APP_ACTIONS.SHOW_SIDE_BAR:
            return {...state, plan: []};
       
        default:
            return state;
    }
}

export default Search;