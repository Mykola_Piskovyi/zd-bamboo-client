import {assign, without, pick, find} from 'lodash';
import LocalStorage from '../Service/LocalStorage';

import {BUILD_ACTIONS, BuildBranch} from '../Actions/Builds.actions';
import {PROJECT_ACTIONS, ProjectPlan} from '../Actions/ProjectPlans.actions';

const initialState: Array<ProjectPlan> = [];

type Action = {
    type: string;
    data: BuildBranch;
};

function projectPlans(state = initialState, action: Action) {
    let data;
    switch (action.type) {
        case PROJECT_ACTIONS.ADD_ALL:
			return action.data;
        default:
            return state;
    }
}

export default projectPlans;