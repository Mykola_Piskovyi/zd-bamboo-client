import {Reducer, combineReducers, ReducersMapObject} from 'redux';

import Builds from './Builds';
import ChainBuilds from './ChainBuilds';
import ProjectPlans from './ProjectPlans';
import Search from './Search';
import App, {AppState} from './App';

export interface ApplicationState {
	app: AppState;
	builds: Array<any>;
	chainBuilds: Array<any>;
	projectPlans: Array<any>;
	searchBranch: any;
}

const reducers = combineReducers({
	builds: <any>Builds,
	chainBuilds: <any>ChainBuilds,
	projectPlans: <any>ProjectPlans,
	search: <any>Search,
	app: <any>App
}) as Reducer<ApplicationState>;

export default reducers;
