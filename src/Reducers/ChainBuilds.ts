import {assign, without, pick, find, values, map} from 'lodash';
import LocalStorage from '../Service/LocalStorage';

import {CHAIN_BUILDS_ACTIONS, ChainBuildBranch} from '../Actions/ChainBuilds.actions';

const initialState: Array<ChainBuildBranch> = LocalStorage.getChainBuilds();

type Action = {
    type: string;
    data: any;
};

function chainBuilds(state = initialState, action: Action) {
    let data;
    switch (action.type) {
        case CHAIN_BUILDS_ACTIONS.ADD_ITEM:
            if (find(state, {key: action.data.key})) {
                return state;
            } else {
                data = [...state, action.data];
                saveToLocalStorage(data);
                return data;
            }
        case CHAIN_BUILDS_ACTIONS.REMOVE_ITEM:
            data = rejectBuilds(state, [action.data]);
            saveToLocalStorage(data);
            return data;
        case CHAIN_BUILDS_ACTIONS.BUILD_STATUS:
        case CHAIN_BUILDS_ACTIONS.LAST_RESULT:
                data = enrichLastBuildStatus(state, action.data);
                saveToLocalStorage(data);
                return data;
        case CHAIN_BUILDS_ACTIONS.ADD_ALL:
			return action.data;
        default:
            return state;
    }
}

export default chainBuilds;

function enrichWithBranchInfo(state, data: ChainBuildBranch) {
    const info = state
        .filter(item => item.key === data.key)
        .map(item => pick(item, LocalStorage.CUSTOM_BUILD_PROPS))
        .reduce((res, props) => (assign(res, props)), {});

    return assign({}, data, info);
}

function enrichLastBuildStatus(state, data: ChainBuildBranch) {
    const enrichedData = enrichWithBranchInfo(state, data);
    data = rejectBuilds(state, [data]).concat([enrichedData]);
    return data;
}

function rejectBuilds(state, items: Array<ChainBuildBranch>) {
    const keys = map(items, 'key');
    return state.filter(item => (keys.indexOf(item.key) === -1));
}

function saveToLocalStorage(chainBuilds) {
    LocalStorage.setChainBuilds(chainBuilds);
}