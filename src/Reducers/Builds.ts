import {assign, without, pick, find, map} from 'lodash';
import LocalStorage from '../Service/LocalStorage';

import {BUILD_ACTIONS, BuildBranch} from '../Actions/Builds.actions';

const initialState: Array<BuildBranch> = getDefaultWatchBuidls();

type Action = {
    type: string;
    data: BuildBranch;
    items?: Array<BuildBranch>;
    branchLink?: string;
    branchRepoName?: string|undefined;
};

function builds(state = initialState, action: Action) {
    let data;
    switch (action.type) {
        case BUILD_ACTIONS.ADD_ITEM:
            if (find(state, {key: action.data.key})) {
                return state;
            } else {
                data = [...state, enrichWithBranchInfo(state, action.data)];
                saveToLocalStorage(data);
                return data;
            }
        case BUILD_ACTIONS.REMOVE_ITEM:
            data = rejectBuilds(state, [action.data]);
            saveToLocalStorage(data);
            return data;
        case BUILD_ACTIONS.REMOVE_ITEMS:
            data = rejectBuilds(state, action.items);
            saveToLocalStorage(data);
            return data;
        case BUILD_ACTIONS.BUILD_RUN:
        case BUILD_ACTIONS.BUILD_STATUS:
        case BUILD_ACTIONS.LAST_RESULT:
            data = enrichLastBuildStatus(state, action.data);
            saveToLocalStorage(data);
            return data;
        case BUILD_ACTIONS.ADD_BRANCH_LINK:
            data = enrichBranchLink(state, action.data, action.branchLink);
            saveToLocalStorage(data);
            return data;
        case BUILD_ACTIONS.ADD_BRANCH_NAME:
            data = enrichBranchRepoName(state, action.data, action.branchRepoName);
            saveToLocalStorage(data);
            return data;
        default:
            return state;
    }
}

export default builds;

function enrichWithBranchInfo(state, data: BuildBranch) {
    const info = state
        .filter(item => item.shortName === data.shortName)
        .map(item => pick(item, LocalStorage.CUSTOM_BUILD_PROPS))
        .reduce((res, props) => (assign(res, props)), {});

    return assign({}, data, info);
    
}

function enrichPropertyName(state, data: BuildBranch, properties: Object) {
    const items = state
        .filter(item => item.shortName === data.shortName)
        .map(item => assign(item, properties));

    return rejectBuilds(state, items).concat(items);
}

function enrichBranchLink(state, data: BuildBranch, branchLink: string) {
    return enrichPropertyName(state, data, {branchLink});
}

function enrichBranchRepoName(state, data: BuildBranch, branchRepoName: string) {
    return enrichPropertyName(state, data, {branchRepoName});
} 

function enrichLastBuildStatus(state, data: BuildBranch) {
    const enrichedData = enrichWithBranchInfo(state, data);
    data = rejectBuilds(state, [data]).concat([enrichedData]);
    return data;
}

function rejectBuilds(state, items: Array<BuildBranch>) {
    const keys = map(items, 'key');
    return state.filter(item => (keys.indexOf(item.key) === -1));
}

function getDefaultWatchBuidls() {
    return LocalStorage.getBuilds();
}

function saveToLocalStorage(builds) {
    LocalStorage.saveBuilds(builds);
}