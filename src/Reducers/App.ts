import {assign, pick} from 'lodash';

import LocalStorage from '../Service/LocalStorage';
import {APP_ACTIONS, NotificationData} from '../Actions/App.actions';
import {BUILD_ACTIONS} from '../Actions/Builds.actions';

export type AppState = {
    activeSection: string;
    signedIn: boolean;
    groupedByBranch: boolean;
    showSideBar: boolean;
    notifications: Array<any>;
};

const initialState: AppState = assign({
    activeSection: 'Dashboard',
    signedIn: false,
    groupedByBranch: false,
    showSideBar: false,
    notifications: []
}, LocalStorage.getApp());

type Action = {
    type: string;
    data: string;
};

function App(state = initialState, action: Action) {
    let data;
    switch (action.type) {
        case APP_ACTIONS.GROUP_BUILDS:
            data = {...state, groupedByBranch: action.data};
            saveAppToLocalStorage(data);
            return data;
        case APP_ACTIONS.SHOW_SIDE_BAR:
            data = {...state, showSideBar: !state.showSideBar};
            saveAppToLocalStorage(data);
            return data;
        case APP_ACTIONS.BAMBOO_SINGED_IN:
        case APP_ACTIONS.BAMBOO_SINGED_OUT: 
            return {...state, signedIn: action.data};
        case APP_ACTIONS.SET_MENU_ITEM:
            return assign({}, state, {activeSection: action.data});
        case BUILD_ACTIONS.SHOW_NOTIFICATION:
            return {...state, notifications: [action.data]};
        case BUILD_ACTIONS.HIDE_NOTIFICATION:
            const notifications = state.notifications
                .filter(item => item.id !== action.data);
            return {...state, notifications: notifications};
        default:
            return state;
    }
}

export default App;

function saveAppToLocalStorage(state) {
    LocalStorage.saveApp(
        pick(state, ['groupedByBranch', 'showSideBar'])
    );
}