import * as actions from '../Actions';

export {
	bindDispatch
}

function bindDispatch(actions: Object, dispatch: Function) {
    const actionMapDispatcher = {};

    Object
        .keys(actions)
        .forEach(key => {
            const binded = {};
            Object
                .keys(actions[key])
                .forEach(action => {
                    binded[action] = (...data) => {
                        const event = actions[key][action](...data);
                        dispatch(event);
                    }
                });
            
            actionMapDispatcher[key] = binded;
        });

    return actionMapDispatcher;
}