import {first, assign, includes} from 'lodash';
import Timeout from 'timeout-exec';
import BambooApi from '../Service/BambooApi';
import {BuildBranch, mapPlanInfoToBranchInfo} from './Builds.actions';

import * as SETTINGS from '../../core/settings';

export type ProjectPlan = {
	enabled: boolean;
	key: string;
	name: string;
	shortName: string;
	planKey?: string;
	type: 'chain';
}

export type ChainBuildBranch = {
	branchName: string;
	description: string;
	key: string;
	planName: string;
	projectName: string;
	type: 'chain';
}

export const CHAIN_BUILDS_ACTIONS = {
	SYNC: 'CHAIN_SYNC',
	ADD_ITEM: 'CHAIN_ADD_ITEM',
    ADD_ALL: 'CHAIN_ADD_ALL',
	REMOVE_ITEM: 'CHAIN_REMOVE_ITEM',
	LAST_RESULT: 'CHAIN_LAST_RESULT',
	BUILD_STATUS: 'CHAIN_BUILD_STATUS',
	SEARCH_RESPONSE: 'CHAIN_SEARCH_RESPONSE',
	SEARCH_REMOVE: 'CHAIN_SEARCH_REMOVE'
};

export const chainBuilds = {
	addAll: (data) => ({type: CHAIN_BUILDS_ACTIONS.ADD_ALL, data: data}),
	searchPlans: searchPlans,
	searchPlansRemove: () => ({type: CHAIN_BUILDS_ACTIONS.SEARCH_REMOVE, data: []}),
	addChainBuild: addChainBuild,
	removeChainBuild: removeChainBuild,
	getLatestChainResult: getLatestChainResult
};

const REFRESH_TIME_PERIOD = SETTINGS.BUILDS.REFRESH_TIME_PERIOD;
const REFRESH_STATUS_AFTER_BUILD_PERION = SETTINGS.BUILDS.REFRESH_STATUS_AFTER_RUN_BUILD;

const api = new BambooApi();
const timeout = new Timeout();

function searchPlans(value: string) {
	return (dispatch) => {
		api.searchPlans(value)
			.then(data => {
				const plans = data.map(item => item.searchEntity);
				dispatch({type: CHAIN_BUILDS_ACTIONS.SEARCH_RESPONSE, data: plans});
			});
	}
}

function addChainBuild(data) {
	return (dispatch) => {
		dispatch({type: CHAIN_BUILDS_ACTIONS.ADD_ITEM, data: data});
		dispatch(getLatestChainResult(data));
	}
};

function removeChainBuild(data) {
	return (dispatch) => {
		dispatch({type: CHAIN_BUILDS_ACTIONS.REMOVE_ITEM, data: data});
		timeout.clearKey(data.key);
	}
}

function getLatestChainResult(item) {
	return (dispatch) => {
		api.getLatestResult(item.key)
		.then((data) => {
			const result = first(data);
			return result || Promise.reject(result);
		})
		.then((result) => {
			let info = mapPlanInfoToBranchInfo(result);

			dispatch({
				type: CHAIN_BUILDS_ACTIONS.LAST_RESULT, 
				data: info
			});

			if (includes(['InProgress', 'Queued'], info.lifeCycleState)) {
				dispatch(getBranchStatus(info));
			}
		})
		.catch(error => {
			console.warn(error);
			dispatch({
				type: CHAIN_BUILDS_ACTIONS.LAST_RESULT,
				data: {...item, status: 'Unknown'}
			});
		});
	}
}

function getBranchStatus(item) {
	return (dispatch) => {
		api.getLatestStatus(item.lastBuildKey)
		.then((data) => {
			dispatch({
				type: CHAIN_BUILDS_ACTIONS.BUILD_STATUS, 
				data: {...item, progress: data.progress}
			});

			if (data.progress) {
				timeout.timer(REFRESH_TIME_PERIOD, item.key)
					.execute(() => (dispatch(getBranchStatus(item))));
			} else {
				delete item.lifeCycleState;
				console.info('DONE: ', item);
				dispatch(getLatestChainResult(item));
			}
		})
		.catch(() => {
			dispatch(getLatestChainResult(item));
		})
	}
}
