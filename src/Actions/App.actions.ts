import {first, assign} from 'lodash';
import BambooApi from '../Service/BambooApi';
import Timeout from 'timeout-exec';

import * as SETTINGS from '../../core/settings';
const NOTIFICATION_PERIOD = SETTINGS.BUILDS.NOTIFICATION_PERIOD;

export type NotificationData = {
	id: string;
	type?: string|undefined;
	text: string;
};

export const APP_ACTIONS = {
	BAMBOO_SINGED_IN: 'BAMBOO_SINGED_IN',
	BAMBOO_SINGED_OUT: 'BAMBOO_SINGED_OUT',
	SET_MENU_ITEM: 'SET_MENU_ITEM',
	SHOW_SIDE_BAR: 'SHOW_SIDE_BAR',
	GROUP_BUILDS: 'GROUP_BUILDS',
	SHOW_NOTIFICATION: 'SHOW_NOTIFICATION',
	HIDE_NOTIFICATION: 'HIDE_NOTIFICATION'
};

export const application = {
	auth: auth,
	getAppAuthStatus: getAppAuthStatus,
	groupBuilds: (data: boolean) => ({type: APP_ACTIONS.GROUP_BUILDS, data: data}),
	signedIn: (data) => ({type: APP_ACTIONS.BAMBOO_SINGED_IN, data: data}),
	setMenuItem: (data: string) => ({type: APP_ACTIONS.SET_MENU_ITEM, data: data}),
	showSideBar: (data) => ({type: APP_ACTIONS.SHOW_SIDE_BAR, data: data}),
	showNotification: (data: NotificationData) => ({type: APP_ACTIONS.SHOW_NOTIFICATION, data: data}),
	hideNotification: (id) => ({type: APP_ACTIONS.HIDE_NOTIFICATION, data: id})
};

const api = new BambooApi();
const timeout = new Timeout();

function toggleNotification(text) {
	const key = 'app_notify';
	return (dispatch) => {
		dispatch(application.showNotification({id: key, text: text}));
		timeout.timer(NOTIFICATION_PERIOD, key)
			.execute(() => (dispatch(application.hideNotification(key))));
	}
}

function auth(credentials: {username: string, password: string}) {
	return (dispatch) => {
		api.auth(credentials)
			.then(data => {
				if (api.hasAuth()) {
					dispatch(application.signedIn(true));
				}
			})
			.catch(error => {
				notifyError(error);
				dispatch(application.signedIn('fail'));
			});

		function notifyError(error) {
			const message = catchErrorMessage(error);
			if (message) {
				dispatch(toggleNotification(message));
			}
		}
	}
}

function getAppAuthStatus() {
	return (dispatch) => {
		if (api.hasAuth()) {
			api.boot()
				.then(data => {
					dispatch(application.signedIn(true));
				})
				.catch(() => {
					dispatch(application.signedIn(false));
				});
		} else {
			dispatch(application.signedIn(false));
		}
	}
}

function catchErrorMessage(errorData: any) {
	const error: any = (typeof errorData === 'object' && errorData.message) ?
		errorData : getErrorFromString(errorData);
	return error.message ? error.message : null;
}

function getErrorFromString(error: string|any): any {
	let result = {message: error};
	try {
		result.message = typeof error === 'string' ? JSON.parse(error) : JSON.stringify(error);
	} catch (e) {}
	return result;
}