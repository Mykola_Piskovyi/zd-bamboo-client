import {first, assign} from 'lodash';
import BambooApi from '../Service/BambooApi';
import {BuildBranch} from './Builds.actions';

export type ProjectPlan = {
	enabled: boolean;
	key: string;
	name: string;
	shortName: string;
	planKey?: string;
	type: 'chain';
}

export const PROJECT_ACTIONS = {
	SYNC: 'SYNC',
    ADD_ALL: 'ADD_ALL',
	REMOVE_ITEM: 'REMOVE_ITEM'
};

export const projectPlans = {
	getAll: getAll,
	addAll: (data) => ({type: PROJECT_ACTIONS.ADD_ALL, data: data})
};

const api = new BambooApi();

function getAll() {
	return (dispatch) => {
		api.getAllPlans()
			.then(data => {
				const projects = data.plans.plan
					.filter(item => item.enabled);
				dispatch(projectPlans.addAll(projects));
			});
	}
}
