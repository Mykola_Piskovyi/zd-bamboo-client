import {first, assign, isEmpty, attempt, isError, map, includes} from 'lodash';
import BambooApi from '../Service/BambooApi';
import Timeout from 'timeout-exec';

import * as SETTINGS from '../../core/settings';

import {NotificationData} from './App.actions';

export type BuildBranch = {
	ticket?: string;
	branchRepoName?: string;
	branchesName?: Array<string>;
	key: string;
	shortName: string;
	projectName: string;
	projectKey: string;
	planName: string;
	status: 'Successful'|'Failed'|'Unknown'|undefined;
	lastBuildKey: string|undefined;
	lifeCycleState: 'InProgress'|'Finished'|'Queued'|'NotBuilt'|undefined;
	progress: any|undefined;
	link: string|undefined;
	branchLink?: string|undefined;
}

export const BUILD_ACTIONS = {
	SYNC: 'SYNC',
    ADD_ITEM: 'ADD_ITEM',
	REMOVE_ITEM: 'REMOVE_ITEM',
	REMOVE_ITEMS: 'REMOVE_ITEMS',
	SEARCH_RESPONSE: 'SEARCH_RESPONSE',
	SEARCH_REMOVE: 'SEARCH_REMOVE',
	ADD_BRANCH_LINK: 'ADD_BRANCH_LINK',
	ADD_BRANCH_NAME: 'ADD_BRANCH_NAME',
	BUILD_RUN: 'BUILD_RUN',
	BUILD_STATUS: 'BUILD_STATUS',
	LAST_RESULT: 'LAST_RESULT',
	BUILD_RESP_ERROR: 'BUILD_RESP_ERROR',
	SHOW_NOTIFICATION: 'SHOW_NOTIFICATION',
	HIDE_NOTIFICATION: 'HIDE_NOTIFICATION'
};

const EMPTY_EVENT = {type: ''};

export const builds = {
	addBuild: addBuild,
	removeBuild: removeBuild,
	removeBranchBuilds: removeBranchBuilds,
	searchBuild: searchBuild,
	searchRemove: (data) => ({type: BUILD_ACTIONS.SEARCH_REMOVE, data: []}),
	addBuildBranchLink: (item, branchLink: string) => ({type: BUILD_ACTIONS.ADD_BRANCH_LINK, data: item, branchLink}),
	setBranchRepoName: (item, branchRepoName: string|undefined) => ({type: BUILD_ACTIONS.ADD_BRANCH_NAME, data: item, branchRepoName}),
	buildPlan: buildPlan,
	getBuildPlan: getBuildPlan,
	createBuildPlan: createBuildPlan,
	getLatestResult: getLatestResult,
	getBranchStatus: getBranchStatus,
	showNotification: (data: NotificationData) => ({type: BUILD_ACTIONS.SHOW_NOTIFICATION, data: data}),
	hideNotification: (id) => ({type: BUILD_ACTIONS.HIDE_NOTIFICATION, data: id})
};

export {
	mapPlanInfoToBranchInfo
};

const REFRESH_TIME_PERIOD = SETTINGS.BUILDS.REFRESH_TIME_PERIOD;
const REFRESH_STATUS_AFTER_BUILD_PERION = SETTINGS.BUILDS.REFRESH_STATUS_AFTER_RUN_BUILD;
const NOTIFICATION_PERIOD = SETTINGS.BUILDS.NOTIFICATION_PERIOD;

const api = new BambooApi();
const timeout = new Timeout();

function addBuild(data) {
	return (dispatch) => {
		dispatch({type: BUILD_ACTIONS.ADD_ITEM, data: data});
		dispatch(getLatestResult(data, {needVcsBranch: true}));
	}
};

function removeBuild(data) {
	return (dispatch) => {
		dispatch({type: BUILD_ACTIONS.REMOVE_ITEM, data: data});
		timeout.clearKey(data.key);
	}
}

function removeBranchBuilds(items: Array<BuildBranch>) {
	return (dispatch) => {
		dispatch({type: BUILD_ACTIONS.REMOVE_ITEMS, items: items});
		items.forEach(item => timeout.clearKey(item.key));
	}
}

function toggleNotification(item, text) {
	return (dispatch) => {
		dispatch(builds.showNotification({id: item.key, text: text}));
		timeout.timer(NOTIFICATION_PERIOD, item.key)
			.execute(() => (dispatch(builds.hideNotification(item.key))));
	}
}

function getBuildPlan(projectBuild, item, branchRepoName?) {
	return (dispatch) => {
		api.getBranchPlan(projectBuild.key, item.shortName)
			.then((build: BuildBranch) => {

				if (branchRepoName) {
					assign(item, {branchRepoName});
				}

				if (build) {
					const data = mapPlanBuildToBranchInfo(build);
					dispatch(addBuild(data));
				} else {
					dispatch(createBuildPlan(projectBuild, item));
				}
			})
			.catch(error => {
				console.warn(error);
				dispatch(EMPTY_EVENT);
			});
	}
}

function buildPlan(item) {
	return (dispatch) => {
		api.buildPlan(item.key)
			.then(data => {
				dispatch({
					type: BUILD_ACTIONS.BUILD_RUN,
					data: assign({}, item, {lifeCycleState: 'InProgress'})
				});

				timeout.timer(REFRESH_STATUS_AFTER_BUILD_PERION, item.key)
					.execute(() => (dispatch(getLatestResult(item))));
			})
			.catch(error => {
				const message = catchErrorMessage(error, item);
				if (message) {
					dispatch(toggleNotification(item, message));
				}
			});
	}
}

type CreatedPlanInfo = {
	enabled: boolean;
	key: string;
	link: any;
	name: string; // Dev Zoomdata - UI EDC Regression - dev-zp-3382-extend-rest-api-visdefs
	shortKey: string; // ESUR195
	shortName: string; // dev-zp-3382-extend-rest-api-visdefs
};

function createBuildPlan(projectBuild, item) {
	return (dispatch) => {		
		api.validateBranchNameBeforeCreatePlan(projectBuild.key, item.shortName, item.branchRepoName)
			.then(({isValid, message}) => {
				if (!isValid) {
					notifyError(message);
					dispatch(builds.setBranchRepoName(item, undefined))
					return false;
				}
				api.createBranchPlan(projectBuild.key, item.shortName, item.branchRepoName)
					.then((item: CreatedPlanInfo) => {
						const data = mapPlanBuildToBranchInfo(item);
						dispatch(addBuild(data));
					})
					.catch(error => notifyError(error));
			})
			.catch(error => notifyError(error));

		function notifyError(error) {
			const message = catchErrorMessage(error, item);
			if (message) {
				dispatch(toggleNotification(item, message));
			}
		}
	}
}

export type SearchResultBuild = {
	entity: {
		branchName: string;
		key: string;
		planName: string;
		projectName: string;
	};
	id: number;
	type: 'BRANCH';
};
function searchBuild(query) {
	return (dispatch) => {
		api.search(query)
			.then((data: Array<SearchResultBuild>) => {
				const builds = data
					.filter(item => (item.type === 'BRANCH'))
					.map(item => item.entity)
					.map(item => {
						return {...item,
							ticket: getTicket(item.branchName),
							shortName: item.branchName
						};
					});

				dispatch({
					type: BUILD_ACTIONS.SEARCH_RESPONSE, 
					data: builds
				});
			});
	}
}

type GetLastResultOpts = {
	needVcsBranch?: boolean
};
function getLatestResult(item, config: GetLastResultOpts = {}) {
	return (dispatch) => {
		api.getLatestResult(item.key)
		.then((data) => {
			const result = first(data);
			return result || Promise.reject(result);
		})
		.then((result) => {
			let info = mapPlanInfoToBranchInfo(result);

			if (config.needVcsBranch) {
				api.getVcsBranches(info.projectKey)
					.then(resp => {
						info = enrichBranchRepoName(resp.branches.branch, info);
						dispatchAndGoOn();
					})
					.catch(() => {
						dispatchAndGoOn();
					});
			} else {
				dispatchAndGoOn();
			}

			function dispatchAndGoOn() {
				dispatch({
					type: BUILD_ACTIONS.LAST_RESULT, 
					data: info
				});
				if (includes(['InProgress', 'Queued'], info.lifeCycleState)) {
					dispatch(getBranchStatus(info));
				}
			}
		})
		.catch(error => {
			console.warn(error);
			dispatch({
				type: BUILD_ACTIONS.LAST_RESULT, 
				data: {...item, status: 'Unknown'}
			});
		});
	}
}

function getBranchStatus(item) {
	return (dispatch) => {
		api.getLatestStatus(item.lastBuildKey)
		.then((data) => {
			dispatch({
				type: BUILD_ACTIONS.BUILD_STATUS, 
				data: {...item, progress: data.progress}
			});

			if (data.progress) {
				timeout.timer(REFRESH_TIME_PERIOD, item.key)
					.execute(() => (dispatch(getBranchStatus(item))));
			} else {
				delete item.lifeCycleState;
				console.info('DONE: ', item);
				dispatch(getLatestResult(item));
			}
		})
		.catch(() => {
			dispatch(getLatestResult(item));
		})
	}
}

function mapPlanInfoToBranchInfo(planInfo): BuildBranch {
	const ticket = getTicket(planInfo.plan.shortName);
	const info = mapPlanBuildToBranchInfo(planInfo.plan);

	// TODO: Use common bamboo item props
	return assign(info, {
		ticket: ticket,
		masterId: planInfo.id,
		status: planInfo.buildState,
		lastBuildKey: planInfo.key,
		lifeCycleState: planInfo.lifeCycleState,
		link: planInfo.link.href
	}, {});
}

function mapPlanBuildToBranchInfo(plan): any {
	const ticket = getTicket(plan.shortName);
	const projectName = plan.master ? plan.master.name : getProjectName(plan.name);
	const projectKey = plan.master ? plan.master.key : getProjectKey(plan.key);
	const planName = plan.master ? plan.master.shortName : getPlanName(plan.name);
	return {
		ticket: ticket,
		key: plan.key,
		shortName: plan.shortName,
		projectName: projectName,
		projectKey: projectKey,
		planName: planName,
		link: plan.link.href
	}
}

// name: Dev Zoomdata - UI EDC Regression - dev-zp-3382-extend-rest-api-visdefs
// Dev Zoomdata - UI Regression
function getProjectName(name): string {
	const names = name ? name.split(' - ') : [];
	return [String(names[0]), String(names[1])].join(' - ');
}

// key: ZD-BUILD920
// returns: ZD-BUILD
function getProjectKey(key): string {
	return key.replace(/[0-9]/g, '');
}

// UI Regression
function getPlanName(name): string {
	const names = name ? name.split(' - ') : [];
	return String(names[1]);
}

function getTicket(shortName): string {
	const TICKET_PREFIX = 'AQT|ZP|ZD';
	const regTest = new RegExp(`(${TICKET_PREFIX})`+'-?\\d\\w+', 'g');
	const text = shortName.toUpperCase().match(regTest);
	const text2 = shortName.toUpperCase().split('-')
		.filter((value) => (value.match(TICKET_PREFIX) || isFinite(Number(value)))).join('-');

	const ticket = text ? 
		text.toString() : 
		text2.substring(text2.toUpperCase().indexOf(TICKET_PREFIX)).toUpperCase();
	
	return ticket;
}

function enrichBranchRepoName(branches: Array<{name: string}>, info: BuildBranch) {
	const SKIP_VCS_BRANCHES = ['release'];
	const branchesName = map(branches, 'name')
		.filter((name: string) => (!name.match(SKIP_VCS_BRANCHES.join('|'))))
		.filter((name: string) => (name.replace(/\//g, '-') === info.shortName));

	return assign({}, info, {branchesName});
}

type BuildRespError = {
	message: string;
	'status-code'?: number;
} | string;

function catchErrorMessage(errorData: string|BuildRespError, item: BuildBranch) {
	const error: any = (typeof errorData === 'object' && errorData.message) ?
		errorData : getErrorFromString(errorData);
	return error.message ? error.message : null;
}

function getErrorFromString(error: string|any): BuildRespError {
	let result = {message: error};
	try {
		result.message = typeof error === 'string' ? JSON.parse(error) : JSON.stringify(error);
	} catch (e) {}
	return result;
}