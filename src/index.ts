import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as Redux from 'redux';
import * as actions from './Actions';
import Timeout from 'timeout-exec';
import BambooApi from './Service/BambooApi';
import store from './Store';
import AppProvider from './Components/Application';
import {ApplicationState} from './Reducers';

import * as APP_SETTINGS from '../core/settings';

const timeout = new Timeout();

window['store'] = store;

ReactDOM.render(
    React.createElement(AppProvider, {store}), 
    document.getElementById('app-view')
);

onInitialize(store);

function onInitialize(store: Redux.Store<ApplicationState>) {
    store.dispatch(actions.application.getAppAuthStatus());
    store.dispatch(actions.projectPlans.getAll());

    checkBuildStatuses(store);

    timeout.interval(APP_SETTINGS.BUILDS.CHECK_BUILDS_STATUSES, 'app')
        .execute(() => {
            checkBuildStatuses(store);
            store.dispatch(actions.application.getAppAuthStatus());
        });
}

function checkBuildStatuses(store) {
    const {builds, chainBuilds} = store.getState();
    
    builds.filter(item => (item.lifeCycleState !== 'InProgress'))
        .forEach(item => {
            store.dispatch(
                actions.builds.getLatestResult(item)
            );
        });

    chainBuilds.filter(item => (item.lifeCycleState !== 'InProgress'))
        .forEach(item => {
            store.dispatch(
                actions.chainBuilds.getLatestChainResult(item)
            );
        });
}