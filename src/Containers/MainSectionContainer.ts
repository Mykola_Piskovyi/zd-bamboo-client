import {sortBy} from 'lodash';
import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../Actions';
import MainSection from '../Components/MainSection';
import {BuildBranch} from '../Actions/Builds.actions';
import {ChainBuildBranch} from '../Actions/ChainBuilds.actions';
import {ProjectPlan} from '../Actions/ProjectPlans.actions';

import {bindDispatch} from '../Utils';

export default connect(mapStateToProps, mapDispatchToProps)(MainSection);

function mapStateToProps(state) {
    const builds = sortBy(state.builds, (item: BuildBranch) => (item.ticket + item.shortName + item.planName));
    const chainBuilds = sortBy(state.chainBuilds, (item: ChainBuildBranch) => (item.branchName + item.projectName));
    return {
        app: state.app,
        builds: builds,
        chainBuilds: chainBuilds,
        projectPlans: state.projectPlans,
        searchPlanResults: state.search.plan
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: {
            builds: bindActionCreators(actions.builds, dispatch),
            chainBuilds: bindActionCreators(actions.chainBuilds, dispatch)
        }
    }
};