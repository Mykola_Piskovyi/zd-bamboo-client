import * as React from 'react'
import { connect } from 'react-redux'
import * as actions from '../Actions';
import Notifications from '../Components/Notifications';

import {bindDispatch} from '../Utils';

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);

function mapStateToProps(state) {
    return {
        notifications: state.app.notifications
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindDispatch(actions, dispatch)
    }
};