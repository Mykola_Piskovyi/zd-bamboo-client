import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../Actions';
import TopMenu from '../Components/TopMenu';

import {bindDispatch} from '../Utils';

export default connect(mapStateToProps, mapDispatchToProps)(TopMenu);

function mapStateToProps(state) {
    return {
        builds: state.builds,
        searchBranch: state.search.branch,
        signedIn: state.app.signedIn,
        showSideBar: state.app.showSideBar,
        activeSection: state.app.activeSection,
        groupedByBranch: state.app.groupedByBranch
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: {
            application: bindActionCreators(actions.application, dispatch),
            builds: bindActionCreators(actions.builds, dispatch),
            projectPlans: bindActionCreators(actions.projectPlans, dispatch),
        }
    }
};