import {isString, isError, pick, attempt, assign} from 'lodash';
import {BuildBranch} from '../Actions/Builds.actions';
import {ChainBuildBranch} from '../Actions/ChainBuilds.actions';

import * as SETTINGS from '../../core/settings';

const APP_NAME = SETTINGS.APP_NAME;

type AppStorage = {
	groupedByBranch: boolean;
	showSideBar: boolean;
};

type storage = {
	builds?: Array<BuildBranch>;
	chainBuilds?: Array<ChainBuildBranch>;
	app?: AppStorage;
};

const CUSTOM_BUILD_PROPS = ['branchLink', 'branchRepoName', 'branchesName'];
const BUILD_PROPS = [
	'key', 'ticket', 'lastBuildKey', 'shortName', 'projectName', 'planName', 'status', 'branchLink'
].concat(CUSTOM_BUILD_PROPS);

const CHAIN_BUILD_PROPS = [].concat(BUILD_PROPS, 'branchName');

const LocalStorage = {

	BUILD_PROPS: BUILD_PROPS,
	CHAIN_BUILD_PROPS: CHAIN_BUILD_PROPS,
	CUSTOM_BUILD_PROPS: CUSTOM_BUILD_PROPS,

	reset: (): storage => {
		localStorage.removeItem(APP_NAME);
		return localStorage.read();
	},

	saveBuilds: (data: Array<BuildBranch>) => {
		const builds = data.map(item => (pick(item, BUILD_PROPS))) as Array<BuildBranch>;
		save({builds});
	},

	getBuilds: (): Array<BuildBranch> => {
		const data = read();
    	return Array.isArray(data.builds) ? data.builds : defaultKeys().builds;
	},

	setChainBuilds: (data: Array<ChainBuildBranch>) => {
		const chainBuilds = data.map(item => (pick(item, CHAIN_BUILD_PROPS))) as Array<ChainBuildBranch>;
		save({chainBuilds});
	},

	getChainBuilds: (): Array<ChainBuildBranch> => {
		const data = read();
    	return Array.isArray(data.chainBuilds) ? data.chainBuilds : defaultKeys().chainBuilds;
	},

	getApp: (): AppStorage => {
		const data = read();
    	return data.app || {} as AppStorage;
	},

	saveApp: (data: AppStorage) => {
		save({app: data});
	}
}

export default LocalStorage;

function read(): storage {
	const data = localStorage.getItem(APP_NAME) || '{}';
	return isError(attempt(() => JSON.parse(data))) ? {} : JSON.parse(data);
}

function save(data: storage) {
	const _data = JSON.stringify(assign({}, read(), data));
	localStorage.setItem(APP_NAME, _data);
}

function defaultKeys() {
	return {
		builds:[],
		chainBuilds: []
	}
}