import {noop, assign, isEmpty, isObject, isString, first, chain, map} from 'lodash';
import * as qs from 'querystring';

import {auth} from '../../server/utils';
import * as APP_SETTINGS from '../../core/settings';

import Rest from './Rest';

interface apiOptions {
	username?: string;
	password?: string;
}

const {COOKIE_NAME} = APP_SETTINGS.BAMBOO;

const queryParams = (params: Object) => (isEmpty(params) ? '' : ('?' + qs.stringify(params)));

export default class BambooApi {

	private _rest: Rest;
	
	constructor(options: apiOptions = {}) {
		this._rest = new Rest();
	}

	private _url(uri: string, params = {}) {
		return 'bamboo/' + uri + queryParams(params);
	}

	boot() {
		return this._rest
			.get(this._url(''), {
				credentials: 'same-origin'
			});
	}

	hasAuth(): boolean {
		const bambooSesssion = document.cookie.split(';')
			.map(pair => (pair.split('=')[0]))
			.filter(key => (key.match(COOKIE_NAME)))[0];

		return !!bambooSesssion;
	}

	auth(credentials: {username: string, password: string}) {
		const cred = {
			username: auth.btoa(credentials.username),
			password: auth.btoa(credentials.password)
		};
		return this._rest
			.post(this._url(''), {
				body: cred,
				credentials: 'same-origin'
			});
	}

	/**
     * Returns the list of plans, key and names available
     */
	getAllPlans() {
		return this._rest
			.get(this._url('plan', {'max-results': 100, enabled: true}))
			.then((data) => parseData(data));
	}

	/**
     * Returns last build results status (progress, finished): state and number
     */
	getLatestStatus(planKey: string, urlParams?: any) {
		const planUri = this._url('result/status/' + planKey);

		return this._rest
			.get(planUri, {credentials: 'same-origin'})
			.then((data) => (parseData(data)));
	}
	/**
     * Returns latest build results: state and number
     */
	getLatestResult(planKey: string, urlParams?: any) {
		const planUri =  this._url('result/' + planKey, {includeAllStates: true});

		return this._rest
			.get(planUri, {credentials: 'same-origin'})
			.then((data) => {
				return data ? parseData(data).results.result : data;
			});
	}
	
	/**
     * Performs search on Bamboo's entities.
	 * @example of urlParams
     *      os_authType: "basic"
     *      masterPlanKey: "KEY",
     *      IncludeMasterBranch: false
	 */
	search(entityToSearch: string, urlParams?: any) {
		const planUri = this._url('quicksearch', {
			searchTerm: entityToSearch,
			'max-result': 100
		});

		return this._rest
			.get(planUri, {credentials: 'same-origin'})
			.then((data) => {
				return parseData(data).searchResults;
			});
	}

	searchPlans(searchTerm: string) {
		const planUri = this._url('search/plan', {
			searchTerm: searchTerm,
			'max-result': 100
		});

		return this._rest
			.get(planUri, {credentials: 'same-origin'})
			.then((data) => {
				return parseData(data).searchResults;
			});
	}

	/**
     * Fire build execution for specified plan. Effectively, this method adds build to the build queue, so is not
     * guarantied that build would be executed immediately.
     */
	buildPlan(planKey: string, urlParams?: any) {
		const planUri = this._url('plan/' + planKey);

		return this._rest
			.post(planUri, {credentials: 'same-origin'})
			.then((data) => {
				return parseData(data);
			})
			.catch(error => {
				throw parseData(error);
			});
	}

	getVcsBranches(projectKey: string) {
		const planUri = this._url(`plan/${projectKey}/vcsBranches`, {
			'max-result': 500
		});
		return this._rest
			.get(planUri, {credentials: 'same-origin'})
			.then((data) => {
				return parseData(data);
			});
	}

	getBranchPlan(projectKey: string, shortName: string) {
		const planUri = this._url(`plan/${projectKey}/branch/${shortName}`);
		return this._rest
			.get(planUri, {credentials: 'same-origin'})
			.then((data) => {
				return parseData(data);
			});
	}

	validateBranchNameBeforeCreatePlan(projectKey: string, shortName: string, branchRepoName: string): Promise<any> {
		return !branchRepoName ?
			Promise.reject(`${projectKey}\n${shortName}\nBranch repository name should be defined`) :
			this.getVcsBranches(projectKey)
				.then(resp => isValidateBranchRepoName(resp, branchRepoName))
				.then(isValid => ({isValid, message: `${projectKey}\n${shortName}\nBranch repository "${branchRepoName}" not exist`}));
	}

	createBranchPlan(projectKey: string, shortName: string, branchRepoName: string) {
		if (!branchRepoName) {
			return Promise.reject(`${projectKey}\n${shortName}\nBranch repository name should be defined`);
		}
		const planUri = this._url(`plan/${projectKey}/branch/${shortName}`, {
			vcsBranch: branchRepoName,
			enable: true,
			cleanupEnabled: true
		});

		return this._rest
			.put(planUri, {credentials: 'same-origin'})
			.then((data) => {
				return parseData(data);
			});
	}
}

function isValidateBranchRepoName(resp, branchRepoName: string): boolean {
	return chain(resp.branches.branch)
		.map('name')
		.includes(branchRepoName)
		.value();
}

function parseData(data) {
	let json;
	if (data && isString(data)) {
		 try {
			json = JSON.parse(data);
		} catch (parseError) {
			console.error(parseError);
		}
	}

	if (isObject(data)) {
		json = data;
	}

	return json;
}

/**
 * Method checks for errors in error and server response
 * Additionally parsing response body and checking if it contain any results
 *
 * @param {Error|null} error
 * @param {Object} response
 * @returns {Error|Boolean} if error, will return Error otherwise false
 * @protected
 */
function checkErrorsWithResult(error, response) {
	var errors = checkErrors(error, response);

	if (errors !== false) {
		return errors;
	}

	try {
		var body = JSON.parse(response.body);
	} catch (parseError) {
		return parseError;
	}

	var results = body.results;

	if (typeof results === "undefined" || results.result.length === 0) {
		return new Error("The plan doesn't contain any result");
	}

	return false;
}

/**
 * Method checks for errors in error and server response
 *
 * @param {Error|null} error
 * @param {Object} response
 * @returns {Error|Boolean} if error, will return Error otherwise false
 * @protected
 */
function checkErrors(error, response) {
	if (error) {
		return error instanceof Error ? error : new Error(error);
	}

	// Bamboo API enable plan returns 204 with empty response in case of success
	if ((response.statusCode !== 200) && (response.statusCode !== 204)) {
		return new Error("Unreachable endpoint! Response status code: " + response.statusCode);
	}

	return false;
}
