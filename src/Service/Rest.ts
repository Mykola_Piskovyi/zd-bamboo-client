import {noop, assign, partial, isEmpty} from 'lodash';
import * as najax from 'najax';
import * as qs from 'querystring';

const HEADERS = {
	ACCEPT: {'Accept': 'application/json'},
	CONTENT_JSON: {'Content-Type': 'application/json'},
	ACCESS_ORIGIN: {'Access-Control-Allow-Origin': '*'}
};

const queryParams = (params: Object): string => (qs.stringify(params));
const urlWithParams = (url, params): string => (url + '?' + queryParams(params));

interface restOptions {

}

type RestResponce = Promise<any>;

interface RequestOptions extends RequestInit {
	urlParams?: Object;
	[key: string]: any;
}

export default class Rest {

	constructor(options: restOptions = {}) {

	}
	
	get(uri: string, options: RequestOptions = {}): RestResponce {
		const getOpts: RequestInit = assign({}, options, {
			method: 'GET',
			headers: assign({}, HEADERS.ACCEPT)
		});

		const url = isEmpty(options.urlParams) ? uri : urlWithParams(uri, options.urlParams);

		return fetch(url, getOpts)
			.then(handleError)
			.then(resp => (resp.text()));
	}

	post(uri: string, options: RequestOptions = {}) {
		const postOpts: RequestInit = assign({}, options, {
			method: 'POST',
			headers: assign({}, HEADERS.ACCEPT, HEADERS.CONTENT_JSON),
			body: options.body ? JSON.stringify(options.body) : ''
		});

		return fetch(uri, postOpts)
			.then(handleError)
			.then(resp => (resp.text()));
	}

	put(uri: string, options: RequestOptions = {}) {
		const putOpts = assign({}, options, {
			method: 'PUT',
			headers: assign({}, HEADERS.ACCEPT)
		});
		return fetch(uri, putOpts)
			.then(handleError)
			.then(resp => (resp.text()));
	}

	delete(uri: string, options: RequestOptions = {}) {
		const deleteOpts = assign({}, options, {
			method: 'DELETE',
			headers: assign({}, HEADERS.ACCEPT)
		});
		return fetch(uri, deleteOpts)
			.then(handleError)
			.then(resp => (resp.text()));
	}

}

function log(type, data, ...args) {
	try {
		console.log(`%cREST ${type}`, 'color:green', JSON.parse(data), args);
	} catch(error) {
		console.error(`%cREST ${type}`, 'color:red', data, args);
	}
}

function handleError(resp) {
	return resp.ok ?
		resp :
		resp.text().then(text => {
			return Promise.reject(text)
		});
}

function onCatch(error) {
	throw new Error(error);
}