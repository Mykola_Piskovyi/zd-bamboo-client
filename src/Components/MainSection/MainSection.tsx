import * as React from 'react';
import * as Redux from 'redux';
import {isEmpty} from 'lodash';
import { Container, Sidebar, Header, Card } from 'semantic-ui-react';

import * as actions from '../../Actions';
import {AppState} from '../../Reducers/App';
import Builds, {BuildsProps} from '../Builds';
import GroupedBuilds from '../GroupedBuilds';
import ChainBuilds, {ChainBuildsProps} from '../ChainBuilds';

import {BuildBranch} from '../../Actions/Builds.actions';
import {ProjectPlan} from '../../Actions/ProjectPlans.actions';

import './MainSection.scss';

interface MainSectionProps extends BuildsProps, ChainBuildsProps {
	actions: typeof actions;
	app: AppState;
}

interface MainSectionState {
}

export default class MainSection extends React.Component<MainSectionProps, MainSectionState> {

	get className() {return 'App-MainSection'}

    render() {
		const {showSideBar, groupedByBranch} = this.props.app;
		const {actions} = this.props;

		const buildsProps = {
			minimal: false,
			type: 'BRANCH',
			builds: this.props.builds,
			projectPlans: this.props.projectPlans,
			removeBuild: actions.builds.removeBuild,
			removeBranchBuilds: actions.builds.removeBranchBuilds,
			buildPlan: actions.builds.buildPlan,
			getBuildPlan: actions.builds.getBuildPlan,
			addBranchLink: actions.builds.addBuildBranchLink
		};

		const chainBuildsProps = {
			chainBuilds: this.props.chainBuilds,
			searchPlanResults: this.props.searchPlanResults,
			searchPlans: actions.chainBuilds.searchPlans,
			searchPlansRemove: actions.chainBuilds.searchPlansRemove,
			addChainBuild: actions.chainBuilds.addChainBuild,
			removeChainBuild: actions.chainBuilds.removeChainBuild
		};

		const withResults = !isEmpty(this.props.searchPlanResults);
		const classNameSideBar = ['chain-builds-sidebar', withResults ? 'with-results-popup' : ''].join(' ');

		return (
			<Sidebar.Pushable className={this.className}>
				<Sidebar className={classNameSideBar} animation='overlay' visible={showSideBar}>
					<ChainBuilds {...chainBuildsProps}/>
				</Sidebar>
				<Sidebar.Pusher>
				<Container className='watched-builds'>
					<Header as='h2' textAlign='center'>Branch Builds</Header>
					{groupedByBranch ? <GroupedBuilds {...buildsProps}/> : <Builds {...buildsProps}/>}
				</Container>
				</Sidebar.Pusher>
			</Sidebar.Pushable>
		);

	}

}