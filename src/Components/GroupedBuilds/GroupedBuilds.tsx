import * as React from 'react';
import {assign, groupBy, isEmpty, find} from 'lodash';
import { Label, List, Container, Segment, Button, LabelProps } from 'semantic-ui-react'

import * as APP_SETTINGS from '../../../core/settings';

import {BuildBranch} from '../../Actions/Builds.actions';
import Builds, {BuildsProps, getCanRegressionBuildPlans} from '../Builds';
import {BuildType} from '../Build';
import TicketLink from '../Build/TicketLink';
import BuildMenu from '../BuildMenu';

import './GroupedBuilds.scss';

export interface GroupedBuildsProps extends BuildsProps {
	removeBranchBuilds(items: Array<BuildBranch>): void;
}

const {LINKS} = APP_SETTINGS;

const GroupedBuilds: React.SFC<GroupedBuildsProps> = (props) => {

	const groupedBuilds = groupBy(props.builds, 'shortName');

	return (
		<List className='grouped-builds'>
			{GroupedList(groupedBuilds, props)}
		</List>
	)
}

export default GroupedBuilds;

function GroupedList(groupedBuilds, props: GroupedBuildsProps) {
	return Object.keys(groupedBuilds)
		.map(name => {
			const buildsProps = {
				...props,
				minimal: true,
				builds: groupedBuilds[name],
			};

			const item: BuildBranch = find(buildsProps.builds, getBranchLink) || buildsProps.builds[0];

			const labelProps: LabelProps = {
				className: 'header-label',
				icon: 'bitbucket',
				color: 'grey',
				content: name,
				ribbon: true
			};
			const brachHref = getBranchLink(item)
			if (brachHref) {
				assign(labelProps, {
					as: 'a',
					color: 'blue',
					href: brachHref,
					target: '_blank'
				});
			}

			const buildMenuProps = {
				item: buildsProps.builds[0],
				regressionProjects: getCanRegressionBuildPlans(item, props.builds, props.projectPlans, true),
				onDeleteBuildPlan: () => (props.removeBranchBuilds(groupedBuilds[name])),
				onGetBuildPlan: (plan, branchRepoName) => (props.getBuildPlan(plan, item, branchRepoName)),
				onAddBranchLink: (link) => (props.addBranchLink(item, link))
			};

			return (
				<List.Item key={name} className='grouped-build-item'>
					<Segment vertical>
						<Container className='grouped-build-header'>
							<Label {...labelProps}/>
							<Button.Group className='header-controls' basic>
								<TicketLink item={buildsProps.builds[0]} />
								<BuildMenu {...buildMenuProps}/> 
							</Button.Group>
						</Container>
						<List.Content>
							<Builds {...buildsProps}/>
						</List.Content>
					</Segment>
				</List.Item>
			);
		})
}

export function getBranchLink(item): string {
	if (item.branchLink) {
		return item.branchLink;
	}
	if (item.branchRepoName) {
		return (LINKS.BITBUCKET_BRANCH_BROWSE + '/' + item.branchRepoName);
	}
	if (!isEmpty(item.branchesName)) {
		return (LINKS.BITBUCKET_BRANCH_BROWSE + '/' + item.branchesName[0]);
	}
}