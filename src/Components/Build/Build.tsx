import {isUndefined, assign} from 'lodash';
import * as React from 'react';
import { Card, Icon, Button, Label, Progress, Dropdown, ButtonProps, ProgressProps } from 'semantic-ui-react';

import {BuildBranch} from '../../Actions/Builds.actions';
import {ProjectPlan} from '../../Actions/ProjectPlans.actions';
import {getBranchLink} from '../GroupedBuilds';
import BuildMenu from '../BuildMenu';
import TicketLink from './TicketLink';

import * as APP_SETTINGS from '../../../core/settings';
import {RegressionProjectPlan} from '../Builds';

import './Build.scss';

const {LINKS} = APP_SETTINGS;

export type BuildType = 'CHAIN'|'BRANCH';
export interface BuildProps {
	minimal: boolean|undefined;
	item: BuildBranch;
	type: BuildType;
	regressionProjects: Array<RegressionProjectPlan>;
	removeBuild(item: BuildBranch): void;
	buildPlan(item: BuildBranch): void;
	getBuildPlan(project: ProjectPlan, item: BuildBranch, branchRepoName?: string): void;
	addBranchLink(item: BuildBranch, link: string): void;
}

const Build: React.SFC<BuildProps> = ({minimal, type, item, regressionProjects, removeBuild, buildPlan, getBuildPlan, addBranchLink}) => {
	const cardColor = getCardColorState(item);
	const inProgress = item.lifeCycleState === 'InProgress';
	const isChain = type === 'CHAIN';
	const newItem = (inProgress || isChain) ? false : isUndefined(item.lastBuildKey);
	const percentage = item.progress ? item.progress.percentageCompletedPretty : '';

	const buildBtn: ButtonProps = {
		compact: true,
		basic: true,
		icon: <Icon name='check circle' color={cardColor}/>,
		as: 'a',
		href: `${LINKS.BAMBOO_BUILD_BROWSE}/${item.lastBuildKey || item.key}`,
		target: '_blank',
		content: 'Build'
	};

	const removeBtn: ButtonProps = {
		compact: true,
		basic: true,
		floated: 'right',
		as: 'a',
		icon: 'hide',
		onClick:() => removeBuild(item)
	};

	const runPlanBtn: ButtonProps = {
		compact: true,
		basic: true,
		as: 'a',
		icon: 'play',
		onClick: () => (buildPlan(item)),
		content: 'Run Plan'
	};

	const buildMenuProps = {
		item: item,
		regressionProjects: regressionProjects,
		onDeleteBuildPlan: () => (removeBuild(item)),
		onGetBuildPlan: (plan, branchRepoName) => (getBuildPlan(plan, item, branchRepoName)),
		onAddBranchLink: (link) => (addBranchLink(item, link))
	};

	const progressProps: ProgressProps = {
		percent: parseInt(percentage),
		active: true,
		color: 'olive',
		size: 'tiny'
	};

	const progressInfo = (
		<span>
			<Icon name='spinner' size='large' loading /> {percentage}
		</span>
	);

	const metaBranchProps = {
		content: item.shortName
	};

	const branchLink = getBranchLink(item);
	if (branchLink) {
		assign(metaBranchProps, {
			as: 'a',
			className: 'link',
			target: '_blank',
			href: branchLink
		});
	}

	const className = ['App-Build', minimal ? 'minimal' : ''].join(' ');

	return (
		<Card color={cardColor} className={className}>
			<Card.Content>
				<Card.Header>
					{minimal ? null : <TicketLink item={item}/> }
					{minimal ? <Button {...removeBtn}/> : <BuildMenu {...buildMenuProps}/>}
				</Card.Header>
				{minimal ? null : <Card.Meta {...metaBranchProps}/>} 
				<Card.Description>
					<div>{item.projectName}</div>
					<div>{item.key}</div>
				</Card.Description>
			</Card.Content>
			<Card.Content className='build-bottom-controls' extra>
				{inProgress ? <Progress {...progressProps}/> : null}
				<Button {...buildBtn}/>
				{newItem ? <Button {...runPlanBtn}/> : null}
				{inProgress ? progressInfo : null}
			</Card.Content>
		</Card>
	);
}

export default Build;

function getCardColorState(item: BuildBranch) {
	const {lifeCycleState, status} = item;
	switch(status) {
		case 'Successful':
			return 'green';
		case 'Failed':
			return 'red';
		case 'Unknown':
			return lifeCycleState === 'NotBuilt' ? 'grey' : undefined;
		default:
			return;
	}
}