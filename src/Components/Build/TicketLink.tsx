import * as React from 'react';
import { Card, Icon, Button, Label, Progress, Dropdown, ButtonProps, ProgressProps } from 'semantic-ui-react';

import {BuildBranch} from '../../Actions/Builds.actions';

import * as APP_SETTINGS from '../../../core/settings';
const {LINKS} = APP_SETTINGS;

export interface TicketLinkProps {
	item: BuildBranch;
}

const TicketLink: React.SFC<TicketLinkProps> = ({item}) => {
	
	const ticketBtn: ButtonProps = {
		compact: true,
		basic: true,
		icon: 'linkify',
		as: 'a',
		href: `${LINKS.JIRA_TICKET_BROWSE}/${item.ticket}`,
		target: '_blank',
		content: item.ticket
	};

	return <Button {...ticketBtn}/>;
};

export default TicketLink;