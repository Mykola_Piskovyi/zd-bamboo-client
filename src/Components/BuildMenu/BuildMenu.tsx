import * as React from 'react';
import { Button, ButtonProps, Dropdown, Modal, Input, Menu, MenuProps } from 'semantic-ui-react';
import {isEmpty, assign} from 'lodash';

import {BuildBranch} from '../../Actions/Builds.actions';
import {RegressionProjectPlan} from '../Builds';

import AddLinkModal from './AddLinkModal';
import './BuildMenu.scss';

interface BuildMenuProps {
	item: BuildBranch;
	regressionProjects: Array<RegressionProjectPlan>;
	onDeleteBuildPlan(): void;
	onGetBuildPlan(project: RegressionProjectPlan, branchRepoName?: string): void;
	onAddBranchLink(link: string): void;
}

interface MenuState {
	showAddLinkModalItem: boolean;
}

export default class BuildMenu extends React.Component<BuildMenuProps, MenuState> {

	constructor(options) {
		super(options);

		this._onAddLinkClick = this._onAddLinkClick.bind(this);
		this._onSetLink = this._onSetLink.bind(this);
		this._onModalClose = this._onModalClose.bind(this);
		this._askForBranchNameConfirm = this._askForBranchNameConfirm.bind(this);

		this.state = {
			showAddLinkModalItem: false
		};
	}

	_onAddLinkClick() {
		this.setState({showAddLinkModalItem: true});
	}

	_onModalClose() {
		this.setState({showAddLinkModalItem: false});
	}

	_onSetLink(link) {
		this.props.onAddBranchLink(link);
		this._onModalClose();
	}

	_askForBranchNameConfirm(project: RegressionProjectPlan) {
		const {branchesName, branchRepoName} = this.props.item;

		if (branchRepoName) {
			this.props.onGetBuildPlan(project);
		} else {
			let correct = false;

			if (branchesName[0]) {
				correct = window.confirm(`Is branche name "${branchesName[0]}" correct?`);
			}

			const newBranchRepoName = correct ? 
				branchesName[0] :
				window.prompt(`Branch name not defined for current item. Please fill in:`);			

			this.props.onGetBuildPlan(project, newBranchRepoName);
		}
	}
	
	render() {
		const {item, regressionProjects, onDeleteBuildPlan, onGetBuildPlan, onAddBranchLink} = this.props;
		const {showAddLinkModalItem} = this.state;
		const dropdownProps = {
			button: true,
			basic: true,
			compact: true,
			pointing: true,
			className: 'build-dropdown icon',
			icon: 'ellipsis horizontal'
		};

		const planProjects = regressionProjects.map((plan) => {
			const planProps = {
				key: plan.key,
				text: plan.name,
				icon: 'fork',
				disabled: item.status !== 'Successful' || plan.disabled,
				onClick: () => (this._askForBranchNameConfirm(plan))
			};
			return <Dropdown.Item {...planProps}/>;
		});
		
		if (showAddLinkModalItem) {
			const addLinkModalProps = {
				item: this.props.item,
				onSetLink: this._onSetLink,
				onModalClose: this._onModalClose
			}
			return <AddLinkModal {...addLinkModalProps}/>
		}

		return (
			<Dropdown {...dropdownProps}>
				<Dropdown.Menu className="build-dropdown-menu">
					<Dropdown.Item text='Remove' icon='remove' onClick={onDeleteBuildPlan}/>
					<Dropdown.Item text='Set a Link Branch' icon='bitbucket' onClick={this._onAddLinkClick}/>
					<Dropdown.Divider className='menu-divider' />
					{planProjects}
				</Dropdown.Menu>
			</Dropdown>
		);
	}

}