import * as React from 'react';
import {Modal, Button, Input, ModalProps } from 'semantic-ui-react';

import {BuildBranch} from '../../Actions/Builds.actions';
import * as APP_SETTINGS from '../../../core/settings';
const {LINKS} = APP_SETTINGS;

import {getBranchLink} from '../GroupedBuilds';

interface AddLinkModalProps {
	item: BuildBranch;
	onModalClose(): void;
	onSetLink(link: string): void;
}

interface AddLinkModalState {
	link: string|undefined;
	loading: boolean;
	invalid: boolean;
}

class AddLinkModal extends React.Component<AddLinkModalProps, AddLinkModalState> {

	constructor(options) {
		super(options);
		this.state = {
			link: undefined,
			loading: false,
			invalid: false
		};
	}

	render() {
		const {item, onModalClose, onSetLink} = this.props;

		const onSubmit = () => (this._checkLinkAndSet());
		const handleKeyPress = (e) => (e.key === 'Enter' ? onSubmit() : null);

		const props: ModalProps = {
			basic: true,
			defaultOpen: true,
			closeIcon: 'close',
			size: 'small',
			dimmer: 'blurring',
			onUnmount: onModalClose
		};

		const inputProps = {
			fluid: true,
			action: true,
			focus: true,
			error: this.state.invalid,
			placeholder: LINKS.BITBUCKET_BRANCH_BROWSE + '/dev/zp-3237-pivot-derived-fields',
			onKeyPress: handleKeyPress,
			defaultValue: getBranchLink(this.props.item),//this.props.item.branchLink,
			onChange: (e) => (this.setState({link: e.target.value, invalid: false}))
		};

		const buttonProps = {
			type: 'submit',
			content: 'Set',
			loading: this.state.loading,
			onClick: onSubmit
		};

		return (
			<Modal {...props}>
				<Modal.Header>
					{this.props.item.ticket}: Set a link of Pull-Request or Branch 
				</Modal.Header>
				<Modal.Content>
					<Input {...inputProps}>
						 <input /> 
						<Button {...buttonProps}/>
					</Input>
				</Modal.Content>
			</Modal>
		)
	}

	private _checkLinkAndSet() {
		if (this.state.loading) {
			return;
		}

		this.setState({loading: true});
		const link = this.state.link || getBranchLink(this.props.item);

		fetch(link, {
			mode: 'cors',
			redirect: 'follow'
		})
			.then((resp) => {
				resp.ok ? 
					this.props.onSetLink(this.state.link) : 
					this.setState({loading: false, invalid: true});
			})
			.catch(() => {
				this.setState({loading: false, invalid: true});
			});
	}
}

export default AddLinkModal;