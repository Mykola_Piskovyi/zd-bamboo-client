'use strict';

import * as React from 'react';
import * as Redux from 'redux';
import { Provider } from 'react-redux';

import NotificationsContainer from '../Containers/NotificationsContainer';
import TopMenuContainer from '../Containers/TopMenuContainer';
import MainSectionContainer from '../Containers/MainSectionContainer';

import './Application.scss';

const Application = (props) => {
    return (
        <div className='BambooClient'>
            <NotificationsContainer {...props}/>
            <TopMenuContainer {...props} />
            <MainSectionContainer {...props} />
        </div>
    );
}

const AppProvider = ({store}) => {
    return (
        <Provider store={store}>
            <Application />
        </Provider>
    );
}

export default AppProvider;
