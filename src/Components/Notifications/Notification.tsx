import {isUndefined} from 'lodash';
import * as React from 'react';
import {Message, MessageProps} from 'semantic-ui-react';

import {NotificationData} from '../../Actions/App.actions';

export interface NotificationProps {
	item: NotificationData,
	onClose(id: string): void; 
}

const Notification: React.SFC<NotificationProps> = ({item, onClose}) => {
	
	const messageProps: MessageProps = {
		color: 'red',
		onDismiss: () => (onClose(item.id))
	};

	return (
		<Message {...messageProps}>
			<MultilineText message={item.text}/>
		</Message>

	);
}

export default Notification;

function MultilineText({message}) {
	return (
		<Message.Content>
			{message.split('\n').map((text, i) => <div key={i}>{text}</div>)}
		</Message.Content>
	)
}