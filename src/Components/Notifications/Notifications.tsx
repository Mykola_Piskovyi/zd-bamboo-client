import {isUndefined} from 'lodash';
import * as React from 'react';
import {Container, Message, MessageProps} from 'semantic-ui-react';

import {NotificationData} from '../../Actions/App.actions';
import Notification, {NotificationProps} from './Notification';

import './Notifications.scss';

export interface NotificationsProps {
	notifications: Array<NotificationData>;
	onCloseNotification(id: string): void;
	actions: any;
}

const Notifications: React.SFC<NotificationsProps> = ({notifications, actions}) => {
	
	const messageProps = {
		content: 'test'
	};

	const items = notifications
		.map(item => {
			const props = {
				key: item.id,
				item: item,
				onClose: (id) => actions.application.hideNotification(id)
			};
			return <Notification {...props}/>
		});

	return (
		<Container className='notifications-container'>
			{items}
		</Container>
	);
}

export default Notifications;