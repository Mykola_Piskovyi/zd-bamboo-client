import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Menu, Segment, Modal, Form, Button, Message, ModalProps, MessageProps } from 'semantic-ui-react';

import * as APP_SETTINGS from '../../../core/settings';

import './BambooLoginForm.scss';

interface LoginFormProps {
	onSubmit(data): void;
	onClose(): void;
}

type LoginFormState = {
	username: string;
	password: string;
	loading: boolean;
}

const BAMBOO_HOST = APP_SETTINGS.BAMBOO.HOST;

class LoginForm extends React.Component<LoginFormProps, LoginFormState> {

	private _refUsername: Element;
	private _refPassword: Element;

	constructor(options) {
		super(options);

		this.state = {
			username: '',
			password: '',
			loading: false
		};
	}

	render() {
		
		const modalProps: ModalProps = {
			className: 'bamboo-login-form',
			basic: true,
			size: 'small',
			defaultOpen: true,
			closeIcon: 'close',
			dimmer: 'blurring',
			onUnmount: () => (this.props.onClose())
		};

		const formProps = {
			loading: this.state.loading,
			onSubmit: (e) => {
				e.preventDefault();
				this.setState({loading: true});
				this.props.onSubmit({
					username: this.state.username,
					password: this.state.password
				});
			}
		};

		const messageProps: MessageProps = {
			content: <div>Use credentials for <a href={BAMBOO_HOST} target='_blank'>{BAMBOO_HOST}</a></div>
		};

		const loginProps = {
			label: 'Login',
			placeholder: 'Username',
			required: true,
			value: this.state.username,
			onChange: (e) => (this.setState({username: e.target.value}))
		};

		const passwordProps = {
			label: 'Password',
			placeholder: 'Password',
			type: 'password',
			required: true,
			value: this.state.password,
			onChange: (e) => (this.setState({password: e.target.value}))
		};

		return (
			<Modal {...modalProps}>
				<Modal.Content>
					<Message {...messageProps}/>
					<Segment>
						<Form {...formProps}>
							<Form.Input {...loginProps}/>
							<Form.Input {...passwordProps}/>
							<Form.Button type='submit'>Login</Form.Button>
						</Form>
					</Segment>
				</Modal.Content>
			</Modal>
		);
	}
};

export default LoginForm;