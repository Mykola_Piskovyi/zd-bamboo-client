import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {assign, groupBy, isEmpty, find, includes} from 'lodash';
import { Input, Header, List, Container, Segment, Button, LabelProps } from 'semantic-ui-react';

import {ChainBuildBranch} from '../../Actions/ChainBuilds.actions';
import SearchBranchItems from '../TopMenu/SearchBranchItems';

import Builds, {BuildsProps} from '../Builds';

import './ChainBuilds.scss';

export interface ChainBuildsProps {
	chainBuilds: Array<ChainBuildBranch>;
	searchPlanResults: Array<ChainBuildBranch>;
	searchPlans(value: string): void;
	searchPlansRemove(): void;
	addChainBuild(chainBuild: ChainBuildBranch): void;
	removeChainBuild(chainBuild: ChainBuildBranch): void;
}

type ChainBuildsState = {
	query: string;
	searchLoading: boolean;
}

class ChainBuilds extends React.Component<ChainBuildsProps, ChainBuildsState> {

	private _refInput: Element;

	constructor(options) {
		super(options);

		this._onSearchPressKey = this._onSearchPressKey.bind(this);
		this._onSearchChange = this._onSearchChange.bind(this);
		this._onSearchPlanItemClick = this._onSearchPlanItemClick.bind(this);
		this._onRemoveChainBuild = this._onRemoveChainBuild.bind(this);
		this._removeSearchResults = this._removeSearchResults.bind(this);

		this.state = {
			query: '',
			searchLoading: false
		};
	}

	componentWillReceiveProps(nextProps) {
        const state = {};
        if (this.props.searchPlanResults !== nextProps.searchPlanResults) {
            assign(state, {searchLoading: false});
		}

		if (!isEmpty(state)) {
			this.setState(state);
		}
	}

	render() {
		const {chainBuilds, searchPlanResults} = this.props;
		const subscribedChainBuilds = chainBuilds.map(item => (item.key));
		const searchChainResults = searchPlanResults.filter(({key}) => (!includes(subscribedChainBuilds, key)));
		const withResults = !isEmpty(searchChainResults);

		const inputProps = {
			icon: 'search',
			className: 'search-input',
			placeholder: 'Type "build"...',
			loading: this.state.searchLoading,
			ref: (input) => (this._refInput = ReactDOM.findDOMNode(input)),
			onKeyPress: this._onSearchPressKey,
			onChange: this._onSearchChange
		};

		const buildsProps: any = {
			minimal: true,
			projectPlans: [],
			builds: this.props.chainBuilds,
			type: 'CHAIN',
			removeBuild: this._onRemoveChainBuild
		};

		const searchResultProps = {
			items: planResultsToResultItems(searchChainResults),
			parentElement: this._refInput,
			className: 'search-chain-result',
			onItemClick: this._onSearchPlanItemClick,
			onBackgroundClick: this._removeSearchResults
		};

		return (
			<Container className='ChainBuilds'>
				<Input {...inputProps}/>
				<Header as='h3' textAlign='center'>Chain Builds</Header>
				{withResults ? <SearchBranchItems {...searchResultProps}/> : null}
				{!isEmpty(chainBuilds) ? <Builds {...buildsProps}/> : null}
			</Container>
		);
	}

	_onSearchPressKey(proxy, target) {
		if (proxy.key === 'Enter') {
            this.setState({searchLoading: !!this.state.query});
            this._onSearchPlanBranch(this.state.query);
        }
	}

	_onSearchChange(event, data) {
		this.setState({query: data.value});
	}

	private _onSearchPlanBranch(value: string) {
        if (value) {
            this.props.searchPlans(value);
        }
	}

	private _onSearchPlanItemClick(chainBuild: ChainBuildBranch) {
		const item: ChainBuildBranch = chainBuild ? find(this.props.searchPlanResults, {key: chainBuild.key}) : null;
        if (item) {
			this.props.addChainBuild(item);
        }
	}
	
	private _onRemoveChainBuild(chainBuild: ChainBuildBranch) {
		const item: ChainBuildBranch = chainBuild ? find(this.props.chainBuilds, {key: chainBuild.key}) : null;
        if (item) {
			this.props.removeChainBuild(item);
        }
	}
	
	private _removeSearchResults() {
        this.props.searchPlansRemove();
    }
}

export default ChainBuilds;

function planResultsToResultItems(planResults) {
	return planResults.map(item => ({
		key: item.key,
		shortName: item.branchName,
		planName: `${item.planName} [${item.projectName}]`,
	}));

}