import * as React from 'react';
import {assign, groupBy} from 'lodash';
import { Input, Segment, List, Menu, Button } from 'semantic-ui-react';

import {BuildBranch, SearchResultBuild} from '../../Actions/Builds.actions';

import './SearchBranchItems.scss';

type ResultItem = {
	shortName: string; // master or branchName
	planName: string;
};

interface ResultProps {
	items: Array<any>; // Array<ResultItem>;
	onItemClick: any;
	parentElement: Element;
	className: string;
	onBackgroundClick(): void;
}

const ListBranchItem = ({item, onItemClick}) => {
	const itemProps = {
		key: item.key,
		onClick: () => onItemClick(item)
	};
	return (
		<List.Item {...itemProps}>
			<List.Content>
				<List.Header as='a'>{item.ticket}</List.Header>
				<List.Description>{item.planName}</List.Description>
				<List.Description>{item.shortName}</List.Description>
			</List.Content>
		</List.Item>
	);
}

const SearchBranchItems: React.SFC<ResultProps> = (props) => {
	const {onItemClick, items, parentElement, className, onBackgroundClick, ...otherProps} = props;

	const groupedByBranch = groupBy(items, 'shortName');
	const groupedItems = Object
		.keys(groupedByBranch)
		.sort()
		.map(shortName => {
			const childBuilds = groupedByBranch[shortName]
				.map((item, i) => <ListBranchItem key={i} item={item} onItemClick={onItemClick}/>);
			return (
				<List.Item key={shortName} className='sub-list-result-item'>
					<List.Icon name='bitbucket' className='sub-list-header'/>
					<List.Header content={shortName} icon='home' className='sub-list-header'/>
					<List.List children={childBuilds}/>
				</List.Item>
			)
		})

	const listItems = items.map((item, i) => <ListBranchItem key={i} item={item} onItemClick={onItemClick}/>);
	
	const listProps: any = {
		children: groupedItems,
		divided: true,
		relaxed: true,
		...otherProps
	};

	const segmentProps = {
		className: className
	};

	const divStyle = calcStylePosition(parentElement);
	const bgStyle = calcBgStyle();

	return (
		<div style={divStyle} className='search-items-results-container'>
			<Segment {...segmentProps}>
				<List {...listProps} divided relaxed />
			</Segment>
			<div className='backgroud-contriner' style={bgStyle} onClick={onBackgroundClick}></div>
		</div>
	)
};

export default SearchBranchItems;

function calcStylePosition(input: Element): any {
	const top = input.clientTop + input.clientHeight + 5;
	return input ? {
		position: 'absolute',
		top: top,
		right: 5,
		maxHeight: window.outerHeight - input.clientHeight - 100 - top
	} : {};
}

function calcBgStyle(): any {
	return {
		width: window.innerWidth
	}
}