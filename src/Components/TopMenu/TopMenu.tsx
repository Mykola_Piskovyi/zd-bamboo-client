import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {isEqual, includes, isUndefined, isEmpty, assign} from 'lodash';
import { Input, List, Menu, Button, Container, Checkbox, MenuProps, MenuItemProps } from 'semantic-ui-react';

import {BuildBranch} from '../../Actions/Builds.actions';

import BambooLoginForm from '../BambooLoginForm';
import SearchMenuItem from './SearchMenuItem';
import LoginButtonItem from './LoginButtonItem';

import './TopMenu.scss';

interface TopMenuProps {
    actions: any;
    builds: Array<any>;
    searchBranch: Array<any>;
    signedIn: boolean|'fail';
    activeSection: string;
    groupedByBranch: boolean;
    showSideBar: boolean;
}

const SHOW_SIDE_BAR = 'SHOW_SIDE_BAR';
const FAST_MENU_ITEMS: Array<MenuItemProps> = [{
    name: 'Chain Builds',
    icon: 'fork',
    value: SHOW_SIDE_BAR
}, {
    name: 'Pull Requests',
    icon: 'bitbucket',
    href: 'https://bitbucket.org/dashboard/pullrequests'
}];

const MenuLeftItems = ({items, activeMenuItem, showSideBar, onItemClick}) => {
    const menuItems = items
        .map(item => {
            const itemProps = {
                name: item.name,
                key: item.name,
                icon: item.icon,
                disabled: item.disabled,
                active: false
            };
            if (item.href) {
                assign(itemProps, {
                    link: !isUndefined(item.href),
                    href: item.href,
                    target: '_blank'
                })
            } else {
                assign(itemProps, {
                   onClick: () => (onItemClick(item)) 
                });
            }

            if (item.value === SHOW_SIDE_BAR) {
                itemProps.active = showSideBar;
            }

            return <Menu.Item {...itemProps} />
        });
    
    return (
        <div className='left menu'>{menuItems}</div>
    );
};

type TopMenuState = {
    query: string;
    showLoginForm: boolean;
    searchLoading: boolean;
};

class TopMenu extends React.Component<TopMenuProps, TopMenuState> {

    get className() {return 'App-TopMenu'}

    constructor(options) {
        super(options);

        this._handleMenuItemClick = this._handleMenuItemClick.bind(this);
        this._changeGrouped = this._changeGrouped.bind(this);
        this.onSearchPressKey = this.onSearchPressKey.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
        this._onSearchItemClick = this._onSearchItemClick.bind(this);
        this._removeSearchResults = this._removeSearchResults.bind(this);
        this._onBambooLogin = this._onBambooLogin.bind(this);

        this.state = {
            query: '',
            showLoginForm: false,
            searchLoading: false
        };
    }

    componentWillReceiveProps(nextProps) {
        const state = {};
        if (this.props.searchBranch !== nextProps.searchBranch) {
            assign(state, {searchLoading: false});
        }

        if (nextProps.signedIn || nextProps.signedIn === 'fail') {
            assign(state, {showLoginForm: false});
        }

        if (!isEmpty(state)) {
            this.setState(state);
        }
    }
    
    render() {
        const {showLoginForm} = this.state;
        const {builds, searchBranch, activeSection, showSideBar} = this.props;
        const subscribedBuilds = builds.map(item => (item.key));
        const searchBuilds = searchBranch.filter(({key}) => (!includes(subscribedBuilds, key)));

        const searchProps = {
            loading: this.state.searchLoading,
            searchBuilds: searchBuilds,
            onKeyPress: this.onSearchPressKey,
            onChange: this.onSearchChange,
            onItemClick: this._onSearchItemClick,
            onBackgroundClick: this._removeSearchResults
        };

        const loginBtnProps = {
            signed: this.props.signedIn === true,
            onLoginClick: () => {
                this.setState({showLoginForm: true})
            }
        };

        const loginFormProps = {
            onSubmit: (data) => (this._onBambooLogin(data)),
            onClose: () => (this.setState({showLoginForm: false}))
        };

        return (
            <Menu className={this.className} fixed='top'>
                <MenuLeftItems items={FAST_MENU_ITEMS} activeMenuItem={activeSection} showSideBar={showSideBar} onItemClick={this._handleMenuItemClick} />
                <Checkbox label='Grouped' toggle checked={this.props.groupedByBranch} fitted onChange={this._changeGrouped}/>
                <Menu.Menu position='right'>
                    <SearchMenuItem {...searchProps}/>
                    <LoginButtonItem {...loginBtnProps}/>
                </Menu.Menu>
                {showLoginForm ? <BambooLoginForm {...loginFormProps}/> : null} 
          </Menu>
        );
    }

    _handleMenuItemClick(data) {
        if (data.value === SHOW_SIDE_BAR) {
            this.props.actions.application.showSideBar(data.name);
        } else {
            this.props.actions.application.setMenuItem(data.name);
        }
    }

    private _removeSearchResults() {
        this.props.actions.builds.searchRemove();
    }

    onSearchChange(event, data) {
        this.setState({query: data.value});
        this._removeSearchResults();
    }

    onSearchPressKey(proxy, target) {
        if (proxy.key === 'Enter') {
            this.setState({searchLoading: !!this.state.query});
            this._onSearchBranch(this.state.query);
        }
    }

    private _changeGrouped(eproxy, data) {
        this.props.actions.application.groupBuilds(data.checked);
    }

    private _onBambooLogin(data: {usename: string, password: string}) {
        if (!isEmpty(data)) {
            this.props.actions.application.auth(data);
        }
    }

    private _onSearchBranch(value: string) {
        if (value) {
            this.props.actions.builds.searchBuild(value);
        }
    }

    private _onSearchItemClick(item: BuildBranch) {
        if (item) {
            this.props.actions.builds.addBuild(item);
        }
    }
    

}

export default TopMenu;