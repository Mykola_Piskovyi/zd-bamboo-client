import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Menu, Input, InputProps } from 'semantic-ui-react';

import SearchBranchItems from './SearchBranchItems';

interface searchProps {
	loading: boolean;
	searchBuilds: Array<any>;
	onKeyPress(proxy, target): void;
	onChange(event, data): void;
	onItemClick(item): void;
	onBackgroundClick(): void;
}

const SearchMenuItem: React.SFC<searchProps> = ({loading, searchBuilds, onKeyPress, onChange, onBackgroundClick, onItemClick}) => {

	const inputProps: InputProps = {
		icon: 'search',
		transparent: true,
		className: 'search-input',
		placeholder: 'Type "ZP-399" or branch name...',
		loading: loading,
		ref: (input) => (this._refInput = ReactDOM.findDOMNode(input)),
		onKeyPress: onKeyPress,
		onChange: onChange
	};
		
	const searchResultProps = {
		className: 'search-result',
		onItemClick: onItemClick,
		parentElement: this._refInput,
		items: searchBuilds,
		onBackgroundClick: onBackgroundClick
	};
		
	return (
		<Menu.Item fitted={true} active={true}>
			<div className='search-section'>
				<Input {...inputProps}/>
				{searchBuilds.length ? <SearchBranchItems {...searchResultProps}/> : null}
			</div>
		</Menu.Item>
	)
};

export default SearchMenuItem;