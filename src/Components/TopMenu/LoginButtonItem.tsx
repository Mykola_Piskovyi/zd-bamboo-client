import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Menu, Button, Form } from 'semantic-ui-react';

interface LoginProps {
	signed: boolean;
	onLoginClick(): void;
}

const LoginButtonItem: React.SFC<LoginProps> = ({signed, onLoginClick}) => {

	const loginProps = {
		name: !signed ? 'Login' : null,
		icon: !signed ? 'user outline' : 'user',
		onClick: !signed ? onLoginClick : null
	};

	return <Menu.Item {...loginProps}/>;
};

export default LoginButtonItem;