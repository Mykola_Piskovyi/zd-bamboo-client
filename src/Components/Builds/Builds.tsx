import * as React from 'react';
import {assign} from 'lodash';
import { Card } from 'semantic-ui-react'

import Build, {BuildProps, BuildType} from '../Build';
import {BuildBranch} from '../../Actions/Builds.actions';
import {ProjectPlan} from '../../Actions/ProjectPlans.actions';

import * as APP_SETTINGS from '../../../core/settings';

import './Builds.scss';

const REGRESSION = APP_SETTINGS.BAMBOO.CATCH_PLANS.join('|').toLowerCase();	// 'regression|upgrade';
const SKIP_PLAN = APP_SETTINGS.BAMBOO.SKIP_PLANS.join('|').toLowerCase();	//'daily';

export interface BuildsProps {
	minimal: boolean;
	type: any;
	builds: Array<BuildBranch>;
	projectPlans: Array<ProjectPlan>;
	removeBuild(item: BuildBranch): void;
	buildPlan(item: BuildBranch): void;
	getBuildPlan(project: ProjectPlan, item: BuildBranch, branchRepoName?: string): void;
	addBranchLink(item: BuildBranch, link: string): void;
}

export type RegressionProjectPlan = ProjectPlan & {
	disabled: boolean;
};

const Builds: React.SFC<BuildsProps> = (props) => {

	const items = props.builds
		.map(item => {
			const regressionProjects = getCanRegressionBuildPlans(item, props.builds, props.projectPlans);
			const buildProps = {
				item: item,
				key: item.key,
				minimal: props.minimal,
				type: props.type,
				regressionProjects: regressionProjects,
				removeBuild: props.removeBuild,
				buildPlan: props.buildPlan,
				getBuildPlan: props.getBuildPlan,
				addBranchLink: props.addBranchLink
			}
			return <Build {...buildProps}/>
		});

	return (
		<Card.Group className="builds-list" stackable>
			{items}
		</Card.Group>
	)
}

export default Builds;

export function getCanRegressionBuildPlans(item, builds, projectPlans, allowRegression: boolean = false): Array<RegressionProjectPlan> {
	const regressionProjects = projectPlans.filter((plan: ProjectPlan) => {
		const name = plan.shortName.toLowerCase();
        return name.match(REGRESSION) && !name.match(SKIP_PLAN);
    });

	const buildProjectName = item.projectName ? item.projectName.split(' - ')[0] : '';
	const canRunRegression = allowRegression || canRunRegressionPlan(item);
	
	const relatedBuildProjects = builds
		.filter((build) => (build.shortName === item.shortName))
		.map(build => build.projectName);

	const plans = !canRunRegression ? [] :
		regressionProjects
			.filter((plan) => {
				return plan.name.match(buildProjectName) ;
			})
			.map(plan => {
				return assign({}, plan, {
					disabled: relatedBuildProjects.includes(plan.name)
				});
			});

	return plans;
}

function canRunRegressionPlan(item: BuildBranch): boolean {
	return item.planName ? !item.planName.toLowerCase().match(REGRESSION) : false;
}